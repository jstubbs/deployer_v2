# Image: tacc/deployer
#
# docker run -v $(pwd):/conf tacc/deployer:dev --setup
FROM ubuntu:18.04

RUN apt-get update -y && apt-get install -y && apt-get install -y software-properties-common &&  apt-add-repository ppa:ansible/ansible
RUN apt-get update -y && apt-get install ansible python3-pip vim -y

RUN mkdir /cli
ADD cli/requirements.txt /cli/requirements.txt
RUN pip3 install -r /cli/requirements.txt

ADD projects /projects
ADD util /util
ADD cli /cli
RUN chmod +x /cli/*.sh

ADD VERSION /cli/VERSION


ENTRYPOINT ["/cli/deployer.sh"]