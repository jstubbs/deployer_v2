# CIC Deployer #

The CIC Deployer is a command line utility for deploying CIC software platforms on cloud infrastructure.

## Quick start ##
To get started using deployer, the only dependency is Docker. Change into a directory that will contain your
configuration files and run:
```
$ export version=dev
$ docker run  -v $(pwd):/conf tacc/deployer:$version --setup
```

This step pulls down the image and installs a small alias script, `deployer.sh`, in the current working directory. With
the alias installed, simply issue commands directly to the script; for e.g.
```
$ ./deployer.sh --help
```
will display help. The general format is,
```
$ ./deployer -p <project> -i <instance> -t <tenant> -a <action> -d <deployment_file>
```

See the documentation section for more details. The `examples` directory within this repo also contains illustrations
 of how to work with deployer.


## Building the Docker Image ##
The Deployer ships as a Docker image. To build the image from source, for instance, when developing the Deployer project
itseld, execute the following from the root of this repository:
```
$ export TAG=dev
$ docker build -t tacc/deployer:$TAG .
```


## Running Tests ##
Clone the repo and and change into the root directory.
```
$ docker run --rm -it -v $(pwd):/code -v $(pwd)/tests/fakekey:/conf/fakekey -e "PYTHONPATH=$PYTHONPATH:/code" -w="/code" --entrypoint /bin/bash tacc/deployer:<version>

Once in the container:
root@<containerid>:/code# pip3 install -r /code/tests/cli-test/requirements.txt
root@<containerid>:/code# pytest
```


## Documentation ##

https://cic-deployer.readthedocs.io/%   