#!/bin/bash

export ANSIBLE_HOST_KEY_CHECKING=False

ARG=$1

if [ "$ARG" = "--setup" ]; then
  # copy the alis to the mounted directory (/conf) and add executable permission
  cp /cli/alias.sh /conf/deployer
  chmod +x /conf/deployer

  # replace deployer version with contents of version file for this image:
  export deployer_version=`cat /cli/VERSION`
  sed -i "s/<deployer_version>/$deployer_version/g" /conf/deployer
  echo "TACC Deployer CLI installed."

elif [ "$ARG" = "" ]; then
  cd /
  python3 /cli/deployer.py --help

else
  cd /
  python3 /cli/deployer.py $*
fi