#!/bin/bash
#
# This script gets copied to the user's working directory as the file "deployer" when the --setup is called so that
# they have a convenience script they can run to call the actual deployer container.

cmd=$(which docker)

version=$(docker version -f '{{.Server.APIVersion}}')

# check to see if we need to pull the image for the required tag:
# cf., https://stackoverflow.com/questions/30543409/how-to-check-if-a-docker-image-with-a-specific-tag-exist-locally
docker inspect --type=image tacc/deployer:<deployer_version> > /dev/null 2>&1 || docker pull tacc/deployer:<deployer_version> > /dev/null

docker run -it -v /:/host -v $(pwd):/conf -e DOCKER_API_VERSION=$version --rm -v /var/run/docker.sock:/var/run/docker.sock tacc/deployer:<deployer_version> $*
