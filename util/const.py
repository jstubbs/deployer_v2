import os


def get_installed_projects():
    # todo - make more dynamic
    return [
        # jupyterhub
        {"title": "TACC Integrated JupyterHub",
         "id": 'jupyterhub',
         "docs_url": "https://cic-deployer.readthedocs.io/en/latest/users/projects.html#jupyterhub",
         "description": "Customized JupyterHub enabling deeper integration and ease of use of TACC resources from "
                        "within running notebooks.",
        },

        {"title": "TACC Agave API",
         "id": 'agave',
         "docs_url": "https://cic-deployer.readthedocs.io/en/latest/users/projects.html#agave-api",
         "description": "TACC's Agave science-as-a-service API platform. ",
        },

        {"title": "TACC OAuth Gateway",
         "id": 'oauth',
         "docs_url": "https://cic-deployer.readthedocs.io/en/latest/users/projects.html#oauth-gateway",
         "description": "TACC's OAuth Gateway. ",
         }

    ]

# version of the core deployer software
VERSION = os.environ.get('DEPLOYER_VERSION', 'dev')

# base path within the deployer container where the user's config is mounted
CONTAINER_BASE_PATH = '/conf'

# list of projects installed in this instance of deployer
INSTALLED_PROJECTS = [p['id'] for p in get_installed_projects()]

# directory to store temporary ansible files
ANSIBLE_TEMP_DIR = '/_deployer_temp'

