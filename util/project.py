"""
Base project classes; Projects should extend this class and implement the do_action method.


Notes on writing a Project class:
================================

Operator-supplied variables are stored on the following objects on project instances:

  * servers (list) - the list of filtered servers object that contains variables set on hosts. 
  * configs (dict) - a final, flattened configs object that combines both password configs and standard configs
  * args (dict) - The original operator-provided arguments, both from the command line and from files, as well
                  as the flattened configs object.

NOTE: Only configs and servers (and ) are actually made available to the playbook at runtime, so any required 
      configuration must be copied there before executing a playbook.

Projects generally need to do two things:
  1) Audit the configuration provided by the operator and supply optional configs with default values.
  2) Run playbooks that consume the "final" configuration.

For 1), there are usually two sources of configuration -- 
    a) configuration on hosts/servers (e.g., databases) and
    b) configuration for the system not tied to a host. 
    For a), the general strategy is to iterate through the servers checking for roles and/or configuration.
    For b), the general strategy is, for a given property, P, 
      * first check the args object for a supplied value and,
        if not present (and not required), supply the default value from a defaults.py module for the project.
      * then, store the final value in the config object, as this is the object is available to the playbook.
       
    Schematically, this roughly looks like:
        self.configs[P] = self.args.get(P, getattr(defaults, P)) 

"""

import datetime
from distutils.dir_util import copy_tree
import subprocess
import sys

import jinja2
import yaml

from util.const import *
from util.template import ConfigGen

SERVER_TEMPLATE = 'servers.j2'

class Error(Exception):
    def __init__(self, msg = None):
        self.msg = msg
        sys.exit(msg)

class PlaybookRunner(object):
    def __init__(self, name, servers, configs, args, base_dir):
        self.name = name
        self.base_dir = base_dir
        self.servers = servers
        self.configs = configs
        self.args = args

    def copy_to_temp(self, keep_files=False):
        # create a temporary directory with format <project_name>_date
        temp_dir = '{}_{}'.format(self.name, str(datetime.datetime.now()).replace(" ", "").split('.')[0])

        if keep_files:
            # if we want to keep these files, we need to create the temporary directory in the CONTAINER_BASE_PATH
            full_temp_dir = os.path.join(CONTAINER_BASE_PATH, temp_dir)
        else:
            # otherwise, store them in the ANSIBLE_TEMP_DIR
            full_temp_dir = os.path.join(ANSIBLE_TEMP_DIR, temp_dir)
        try:
            os.makedirs(full_temp_dir)
        except Exception as e:
            raise Error("Error creating temporary directory: {}; exception: {}".format(
                os.path.join(ANSIBLE_TEMP_DIR, temp_dir), e))
        # copy the contents of the project directory to the temp directory
        try:
            copy_tree(self.base_dir, full_temp_dir)
        except Exception as e:
            raise Error("Error copying contents from {} to temporary directory {}; exception: {}".format(
                self.base_dir, os.path.join(ANSIBLE_TEMP_DIR, temp_dir)))
        return full_temp_dir

    def get_hosts_role(self, role):
        """
        Returns a list of servers with a given role
        :param role:
        :return:
        """
        return [s for s in self.servers if role in s.get('roles')]

    def get_host_role_count(self, role):
        """
        Count the number of hosts with the specified role, `role`.
        :param role: (str) - the role to check for
        :return: (int) - the number of hosts with the given role.
        """
        return len(self.get_hosts_role(role))

    def check_host_role_exact(self, role, num=1):
        """
        Check whether there is exactly `num` host(s) with role `role`.
        """
        return self.get_host_role_count(role) == num

    def apply_derived_roles(self, role_map):
        """
        Apply derived roles specified in the project's role_map. the role_map object should be a dictionary where
        each key is string which is a role to look for and each value is a list of roles to add to any server that
        has the key role.
        :param role_map:
        :return:
        """
        for role, new_roles in role_map.items():
            # check to see if a host has a role:
            for server in self.servers:
                try:
                    if role in server.get('roles'):
                        # add the other roles:
                        for r in new_roles:
                            if r not in server.get('roles'):
                                server.get('roles').append(r)
                except TypeError:
                    # this should only happen if server.get('roles') is None
                    pass

    def get_tenant_url(self, tenant_name):
        """
        Try to look up the URL associated with a tenant.
        :param tenant: 
        :return: 
        """
        if tenant_name.upper() == 'AGAVE-PROD':
            return 'https://public.agaveapi.co'
        if tenant_name.upper() == 'ARAPORT-ORG':
            return 'https://api.araport.org'
        if tenant_name.upper() == 'DESIGNSAFE':
            return 'https://agave.designsafe-ci.org'
        if tenant_name.upper() == 'DEV-STAGING':
            return 'https://dev.tenants.staging.tacc.cloud'
        if tenant_name.upper() == 'DEV-DEVELOP':
            return 'https://dev.tenants.develop.tacc.cloud'
        if tenant_name.upper() == 'IPLANTC-ORG':
            return 'https://agave.iplantc.org'
        if tenant_name.upper() == 'IREC':
            return 'https://irec.tenants.prod.tacc.cloud'
        if tenant_name.upper() == 'TACC-PROD':
            return 'https://api.tacc.utexas.edu'
        if tenant_name.upper() == 'SD2E':
            return 'https://api.sd2e.org'
        if tenant_name.upper() == 'SGCI':
            return 'https://agave.sgci.org'
        if tenant_name.upper() == 'VDJSERVER-ORG':
            return 'https://vdj-agave-api.tacc.utexas.edu'
        if tenant_name.upper() == '3DEM':
            return 'https://api.3dem.org'
        return None

    def _call_ansible_playbook(self, playbook_file, temp_dir, hosts_file=None):
        """
        Fork ansible-playbook in a child process and collect pipe standard out to the screen.
        :return:
        """
        print("Executing playbook: {}".format(playbook_file))
        if not hosts_file:
            hosts_file = os.path.join(temp_dir, '_deployer_servers.yml')
        try:
            ans_prc = subprocess.Popen(["ansible-playbook", "-i", hosts_file, playbook_file, "-e", "from_deployer=True"],
                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            raise Error("Error: unable to execute playbook {}. "
                        "ansible-playbook command exited. Info: {}".format(playbook_file, e))
        except Exception as e:
            raise Error("Error: unable to execute playbook {}. "
                        "unrecognized exception. Info: {}".format(playbook_file, e))
        for line in iter(ans_prc.stdout.readline, b''):
            sys.stdout.write(line.decode('utf-8'))
        for line in iter(ans_prc.stderr.readline, b''):
            sys.stdout.write(line.decode('utf-8'))

    def execute_playbook(self, playbook, keep_files=False):
        """
        Call ansible-playbook for a given playbook with the playbook runner's servers and config objects.
        :param playbook: (str) path to a playbook, relative to the project's root dir.
        :return:
        """
        # copy project directory contents to the temp dir:
        temp_dir = self.copy_to_temp(keep_files)

        # local directory to copy the extras dir to.
        extras_dir = '_deployer_extras'

        # copy the extras dir to a directory called _deployer_extras within the temp dir
        if self.args.get('extras'):
            try:
                copy_tree(self.args['extras'], os.path.join(temp_dir, extras_dir))
            except Exception as e:
                raise Error("Error copying contents from {} to temporary directory {}; exception: {}".format(
                    self.base_dir, os.path.join(ANSIBLE_TEMP_DIR, temp_dir), e))

        # write the servers object to a file called _deployer_servers.yml within the temp dir
        self.write_host_file(temp_dir)

        # write a host_vars file for each server
        for server in self.servers:
            self.write_host_vars_file(temp_dir, server)

        # write the configuration object to a file called _deployer_configuration.yml within the temp dir
        self.write_config_file(temp_dir)

        # execute the ansible playbook
        self._call_ansible_playbook(playbook_file=os.path.join(temp_dir, playbook), temp_dir=temp_dir)

    def execute_common_playbooks(self, commons):
        """
        Convenience function for executing a list of common playbook runners using this playbook 
        runner's config.
         
        :param common_playbooks: a list of strings with the names of the common playbook runners to execute. 
        :return: 
        """
        for c in commons:
            # create a PlaybookRunner object for the common playbook with self's configs
            playbook_runner = CommonPlaybook(c, self.servers, self.configs, self.args)

            # the playbook name should be the name of the runner plus the extension
            playbook_name = '{}.plbk'.format(c)

            # execute the playbook using self's configs:
            playbook_runner.execute_playbook(playbook=playbook_name,
                                             keep_files=self.args['keep_tempfiles'])

    def _add_properties_to_servers(self):
        """
        This method traverses each server in the project's servers and adds an additional `properties` list which
        can be used in the servers.j2 template to add
        :return:
        """
        # list of fields that are used explicitly in the servers.j2 file or that are explictly not used:
        known_fields = ['host_id', 'ssh_host', 'absolute_path_ssh_key', 'roles', 'ssh_user', 'ssh_key', 'uuid']

        for server in self.servers:
            if hasattr(server, 'properties'):
                continue
            # the list of properties to add for this server
            properties = []

            # iterate through the server's items and add any "unrecognized" properties that are strings;
            for k, v in server.items():
                if type(v) not in [bool, str]:
                    continue
                if k in known_fields:
                    continue
                properties.append({k: v})
            server['properties'] = properties

    def get_hosts(self):
        pass

    def write_host_file(self, temp_dir):
        """
        Determine the unique roles among the servers; For each role, make a group in the servers file
        (i.e., [<role_name>]) and write out all servers that are in that role with all variables attached.
        In general, a given server could appear in multiple groups but this facilitates executing different playbooks
        that expect different roles on hosts.

        :param temp_dir: (str) - Path to the directory (in the deployer container) to write the file.
        :return:
        """
        server_file_name = '_deployer_servers.yml'
        full_server_file_path = os.path.join(temp_dir, server_file_name)

        # create the unique set of roles for these servers:
        roles = {r for s in self.servers for r in s.get('roles')}

        # add the special `properties` attribute to simplify
        self._add_properties_to_servers()
        # print("servers right after _add_properties_to_servers: {}".format(self.servers))

        # build a list of servers for each role
        servers_by_role = []
        for r in roles:
            servers = self.get_hosts_role(r)
            if servers:
                servers_by_role.append({'role_name': r,
                                        'servers': servers})
        context = {'servers_by_role': servers_by_role}
        conf = ConfigGen(SERVER_TEMPLATE)
        env = jinja2.Environment(loader=jinja2.FileSystemLoader('/util'), trim_blocks=True, lstrip_blocks=True)
        try:
            conf.generate_conf(context, full_server_file_path, env)
        except Exception as e:
            raise Error("Error compiling servers template; error: {}".format(e))

    def write_host_vars_file(self, temp_dir, server):
        """
        Write a temporary host vars yml file to enable hosts to leverage complex variable types.
        From the Ansible docs, assuming
          1) the inventory file path is <p>/hosts and
          2) the hostname is h,
        variables in a yaml file at path <p>/host_vars/<h>.yml will be available to the host.

        cf., Ansible docs:
        https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#splitting-out-host-and-group-specific-data

        :param temp_dir: the root directory where the host_vars directory should live.
        :return:
        """
        host_vars_dir = os.path.join(temp_dir, 'host_vars')
        if not os.path.isdir(host_vars_dir):
            try:
                os.makedirs(host_vars_dir)
            except Exception as e:
                raise Error("Error: unable to create the host_vars directory.")
        host_vars_file_path = os.path.join(host_vars_dir, '{}.yml'.format(server['uuid']))
        d = {}
        for k, v in server.items():
            # detect if this key is a complex object
            if type(v) == dict and k in v:
                # if so, just use the value of the key in the object:
                d[k] = v[k]
            else:
                # otherwise, copy it over as is:
                d[k] = v
        with open(host_vars_file_path, 'w') as yaml_file:
            yaml.dump(d, yaml_file, default_flow_style=False)


    def write_config_file(self, temp_dir):
        """
        Write a temporary config file of the flattened configs
        :param temp_dir:
        :return:
        """
        d = {}
        for k, v in self.configs.items():
            # detect if this key is a complex object
            if type(v) == dict and k in v:
                # if so, just use the value of the key in the object:
                d[k] = v[k]
            else:
                # otherwise, copy it over as is:
                d[k] = v
        # print("final config object: {}".format(d))
        with open(os.path.join(temp_dir, '_deployer_configuration.yml'), 'w') as yaml_file:
            yaml.dump(d, yaml_file, default_flow_style=False)

    def get_actions(self):
        """
        This method should be implemented in the child class to
        :return: list of strings that are the names of the actions available in this project.
        """
        raise NotImplementedError

    def do_action(self, action):
        """
        This method should be implemented in the child class. It handles
        :param action: the action, as a str, that should be invoked.
        :return:
        """
        raise NotImplementedError


class CommonPlaybook(PlaybookRunner):
    """
    Used to execute a playbook from the projects/common directory.
    """

    def __init__(self, name, servers, configs, args):
        super(CommonPlaybook, self).__init__(name, servers, configs, args, base_dir='/projects/common/{}'.format(name))


class Project(PlaybookRunner):
    """
    Used to execute a project.
    """

    def __init__(self, name, servers, configs, args):
        super(Project, self).__init__(name, servers, configs, args, base_dir='/projects/{}'.format(name))