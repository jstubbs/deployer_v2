# ------------------------
# User-friendly Attributes
# ------------------------
# The following attributes are available to the operator to define; they map to legacy attributes in the mapping below.
# When sensible, a default value is supplied.

# tenant attributes
tenant = None
base_url = None
tenant_admin_role = "Internal/{}-services-admin" # default to <tenant>-services-admin
apim_admin_pass = 'a2m1np@ss'
deploy_admin_password_grant = True
deploy_admin_services = False
access_token_validity_time = '14400'
deploy_custom_login_app = False
apim_increase_global_timeout = False

oauth_mysql_port = '3306'
oauth_mysql_password = 'rmQ139zV4'
oauth_mysql_root_password = 'rmQ139zV4'
oauth_mysql_user = 'root'

# whether or not to use the custom cic ldap server; must be specified by the operator as true or false. if false,
# ldap configs will be interpreted as for an existing ldap server the operator already has in place.
use_cic_ldap = None

# ldap attributes
oauth_ldap_full_connection_uri = None # includes 'ldap/s://' + port; will be dervied from other properties
# these defaults only apply when using the built-in cic ldap
oauth_ldap_port = '389'
oauth_ldap_protocol = 'ldap'
oauth_ldap_bind_dn = 'cn=admin,dc=agaveapi'
oauth_ldap_base_search_dn = 'dc=agaveapi'
oauth_ldap_bind_password = 'rmQ139zV4'
# these defaults are valid for all ldap's
oauth_ldap_max_user_name_list_length = 100
oauth_ldap_max_role_name_list_length = 100
oauth_ldap_member_of = 'memberOf'
oauth_ldap_group_search_base = 'ou=system'
oauth_ldap_user_search_filter = '(&amp;(objectClass=person)(uid=?))'

# agave attributes
agave_api_protocol = 'http'
agave_frontend_host = None
agave_frontend_sandbox_host = 'agave-api.sandbox.tacc.cloud'

# abaco attributes
abaco_api_protocol = 'http'
abaco_frontend_base_url_or_ip = 'abaco.api.tacc.cloud'

# identity, profiles and clients
profiles_read_only = True
profiles_check_jwt = True
profiles_check_user_admin_role = True
profiles_debug = True

# oauth proxy -
# these variables are paths to the corresponding files on the oauth host. since we generate the files with contents
# filled with the operator configs, the paths themselves will always be the same
oauth_proxy_cert_file = '/home/apim/{}/oauth.cert' # replace with <tenant>
oauth_proxy_cert_key_file = '/home/apim/{}/oauth.key'
oauth_proxy_ca_cert_file = '/home/apim/{}/oauth_ca.cert'

# Services and Images
services = [
    {
        'service': 'apim',
        'image': 'apim',
    },
    {
        'service': 'profiles',
        'image': 'profiles-api',
    },
    {
        'service': 'clients',
        'image': 'clients-api',
    },
    {
        'service': 'proxy',
        'image': 'auth-proxy',
    },
    {
        'service': 'apim_admin',
        'image': 'apim-admin',
    },
]
oauth_docker_registry_org = 'tacc'
# todo -- update when 1.0.0 is released
# oauth_version = '1.0.0'
oauth_version = 'dev'

oauth_lb_image = 'auth-lb'
oauth_migrations_image = 'apim-migrations'


# SSL
oauth_cert = '-----BEGIN CERTIFICATE-----\nMIIDdTCCAl2gAwIBAgIJAMxQBy4f2s0NMA0GCSqGSIb3DQEBCwUAMFExCzAJBgNV\nBAYTAlVTMQ4wDAYDVQQIDAVUZXhhczEPMA0GA1UEBwwGQXVzdGluMSEwHwYDVQQK\nDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwHhcNMTQxMjE3MjAyNTU1WhcNMTUx\nMjE3MjAyNTU1WjBRMQswCQYDVQQGEwJVUzEOMAwGA1UECAwFVGV4YXMxDzANBgNV\nBAcMBkF1c3RpbjEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIB\nIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0U3R9e/AM3S6kF01tzUJqPW3\nLfTKRHRm8uRGY1WmiDTAeI2Djv4ujjLr7AvuK5Dzgj1fX6ZpKwEYNRtraaAwQ8Yk\nU7D+UM8100/KGbo8Hp6wle9Ku8vKAV302sDj68pb0lCNxs+UHu7Jqb90K2juqjpf\nX03EGoE05ZF+KV1XHA653Iv9iMn9T1a/BBEDe05MLqLyld7ZlIntCIPAoOMvQrWz\n85t4yiMJn0NgA1G5AjIXySu9jAepIJB3weCs8dG1NAeKrc6Ls76Ax+lVh4nQSyDS\nLVHCuugYno9clzPxnxOZKUn/X8WNDPnsHW9K0FaG6KA32BJj4/SyMt3+ERQQgwID\nAQABo1AwTjAdBgNVHQ4EFgQU7gfzLI53EALgjUaS3o2QRijPPOAwHwYDVR0jBBgw\nFoAU7gfzLI53EALgjUaS3o2QRijPPOAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0B\nAQsFAAOCAQEAXY6emewmOsi+dbdrUIvlQS95+0h/L7va5gFbldkgS9ZqnlzKaCb0\nxQRA/eWu1MIjrMDzSg889xsvGf6/e0XYjrX73EvGj3Qwp7nz9WwHCtBNMQ7I2M1r\n++nGVzgCWpzGU2+/htHsoD97kYkJIDY1gENULBwb/bJuSZPqcY+dfQAbVGLZ01OJ\nUR4zUZsgbHLnkRX5v196Upzo2Z9xTUpz0Bqy0aXiAuwVWoVMuPWXzwe0mJc1JVLI\nXsIoLcI7w1nS/jkbw+o0KOIx9sO53YDwMxtlKLmZgb/dhfr08QSsUqE5GLWOGQZg\nLb/PnvyRqJ0yAUik+y2EXhRX5WaQ+8YMzQ==\n-----END CERTIFICATE-----\n'
oauth_key = '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDRTdH178AzdLqQ\nXTW3NQmo9bct9MpEdGby5EZjVaaINMB4jYOO/i6OMuvsC+4rkPOCPV9fpmkrARg1\nG2tpoDBDxiRTsP5QzzXTT8oZujwenrCV70q7y8oBXfTawOPrylvSUI3Gz5Qe7smp\nv3QraO6qOl9fTcQagTTlkX4pXVccDrnci/2Iyf1PVr8EEQN7TkwuovKV3tmUie0I\ng8Cg4y9CtbPzm3jKIwmfQ2ADUbkCMhfJK72MB6kgkHfB4Kzx0bU0B4qtzouzvoDH\n6VWHidBLINItUcK66Biej1yXM/GfE5kpSf9fxY0M+ewdb0rQVobooDfYEmPj9LIy\n3f4RFBCDAgMBAAECggEAIksv5a38vfGYR+Md1ADarlZkRyFeqsyvuYhxiAEWoiaS\niq4VKi63jhEF2s0xnu3HGEXaBKANh0BMqRbvCIV4owIqHZRYdwEcfp0KLiD3jMTP\nhiGJCzAezw6H+fe04INtfA46zKJiUJcrb6aw8BPnVvjte34QbY9SDXNDSp/CkBQF\nkFDK8YwWnDYTR4QRuhXMlzcj7sPzZu/kG4jCTj6DQwWZ8oNBTDb4V2Cgavftq7mD\ngiZlh34StuQVaTBNiWeG5VUaVnc80o1JhfRJuv8J+0RDX4YDcI+O7h9iOYZFzqWd\nUSWrUp3U9yVoHl6QrvRtLai4M76KcI1T1DXs/LoGEQKBgQD4fyGi4YB7nFhtg2Lq\nirhVT3LXy862m/6P5PjKUDAnnQDvn7cUuZWJ+ReHOMITNPwOm7TXOsjzofKzTBGm\ngH5lo1na6NelcjRbwaGgMUcgaI+xF15r4qdAlwMOKRs5J8DnzpgvStjpoG1UUevC\ntHrkJnnChJU9Lqdon6iR53pHHQKBgQDXn7s9Laz3Od30fIwea2BCX+K9DOotrY8O\n3DDxqo8K2l+JoSeT/zC+3bJcNcmpDVyUmvFOBsbJNitrvjIKpUXDH6yZCSiZozAe\n8H6Mli67p6zE4CIXakbdI5O8gQ/R9B5KxiTzmO0HoF6lV31CphtVp+2T+VqT96HE\n7+gaU30EHwKBgEV/gM/GE6H+mvid61c/TsiFPNA/ruOBg1OJLU+f16fskCt0VsbU\nFI+O+9aUuCqZAfmv8IbVaQhCfddJahJQE4mnguTnhBBAfYZABE0CGytkMvPLXlv7\n+tzknzUhp/x3MstiC0wGud21QX54nuBIi6otXJZbcEW5O5bWOFX+EEO9AoGABbEf\nvjOwdy4937BR+pOXDOl/EMKAhAgq2JeTay23bO/qybVJWCzKWp97j9eTpTTHFt0o\nNYkUQkd9yfBRXdqTBt2VLs0BF10pbgenL4rfXPDGLMRF4hxaFohT5Av8IOyyyAKH\nAy8sQqFHHlI66kF8QzyVluxkKbfFu28hz2AY0r0CgYBHNaAQ5IP1EKUfnnG1YKXA\n9q8wfEXyHKrMzSvNJ+amMYXrJ5J56/DVaMSJKU8wR3af18zxoW1UDqanWFf3Kqjq\nnPMIvj1fJOUX4uG+4YAcmluQliXQPiv5p4+r5kUjbMGSZZomWaHDHzoNp8qpFznV\nvRrp/EVfaIpNfzQvPJJM3A==\n-----END PRIVATE KEY-----\n'

# default values for legacy configs that do not change but need special formatting;
# these variable names must match the legacy variables names so that the default look up will work.
jwt_header = 'HTTP_X_JWT_ASSERTION_{}' # format with <tenant>
agave_id_user_admin_role = 'Internal/{}-user-account-manager' # format with <tenant>
agave_id_app_base = 'https://{}' # format with <base_url>

# default values for legacy configs that do not change and need no special formatting-
legacy_update_auth_dns = False
legacy_use_hosted_id = True
legacy_use_remote_userstore = False
legacy_use_custom_ldap = False
legacy_agave_id_apim_pub_key = '/home/apim/public_keys/apim_default.pub'
legacy_beanstalk_server = '172.17.0.1'
legacy_beanstalk_port = 55555
legacy_beanstalk_tube = 'xxxxx'
legacy_beanstalk_srv_code = '0001-001'
legacy_beanstalk_tenant_id = 'xxxxx'

# Legacy configs - includes either a static default when the config no longer makes sense to expose to the operator or
# a translation to another default when it is something the operator can modify.
oauth_legacy_configs = {
    'update_auth_dns': 'legacy_update_auth_dns',
    'access_token_validity_time': 'access_token_validity_time',
    'deploy_admin_password_grant': 'deploy_admin_password_grant',
    'tenant_id': 'tenant',
    'host': 'base_url',
    'deploy_custom_oauth_app': 'deploy_custom_login_app',
    'apim_increase_global_timeout': 'apim_increase_global_timeout',

    # ldap --
    'auth_ldap_max_user_name_list_length': 'oauth_ldap_max_user_name_list_length',
    'auth_ldap_max_role_name_list_length': 'oauth_ldap_max_role_name_list_length',
    'auth_ldap_member_of': 'oauth_ldap_member_of',
    'auth_ldap_group_search_base': 'oauth_ldap_group_search_base',
    'auth_ldap_user_search_filter': 'oauth_ldap_user_search_filter',

    # agave --
    'core_api_protocol': 'agave_api_protocol',
    'core_frontend_base_url_or_ip': 'agave_frontend_host',
    'core_frontend_sandbox_base_url_or_ip': 'agave_frontend_sandbox_host',

    # identity, profiles and clients
    # we always set use_hosts_id to True and use_remote_userstore to False -- we only need one set so that the apim
    # userstore will be compiled correctly.
    'use_hosted_id': 'legacy_use_hosted_id',
    'use_remote_userstore': 'legacy_use_remote_userstore',

    'agave_id_read_only': 'profiles_read_only',
    'hosted_id_domain_name': 'hosted_id_domain_name',
    'ldap_name': 'oauth_ldap_full_connection_uri',
    'auth_ldap_bind_dn': 'oauth_ldap_bind_dn',
    'ldap_base_search_dn': 'oauth_ldap_base_search_dn',
    'agave_id_check_jwt': 'profiles_check_jwt',
    'jwt_header': 'jwt_header',
    'agave_id_apim_pub_key': 'legacy_agave_id_apim_pub_key',
    'agave_id_user_admin_role': 'agave_id_user_admin_role',
    'agave_id_check_user_admin_role': 'profiles_check_user_admin_role',
    'use_custom_ldap': 'legacy_use_custom_ldap',
    'agave_id_app_base': 'agave_id_app_base',
    'agave_id_debug': 'profiles_debug',

    # beanstalk integration is deprecated
    'beanstalk_server': 'legacy_beanstalk_server',
    'beanstalk_port': 'legacy_beanstalk_port',
    'beanstalk_tube': 'legacy_beanstalk_tube',
    'beanstalk_srv_code': 'legacy_beanstalk_srv_code' ,
    'beanstalk_tenant_uuid': 'legacy_beanstalk_tenant_id',
}

# databases
mysql_root_user = 'root'
mysql_login_host = '172.17.0.1'
mysql_host_port = '3306'
mysql_version = '5.6'
