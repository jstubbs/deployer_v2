
from util.project import Project, Error
from . import defaults

class Oauth(Project):
    """
    Project class to handle deployment actions for the OAuth gateway project.
    """

    DBs = [('mysql', 'MySQL'),
           ('ldap', 'LDAP'),
           ]

    EXTRA_APIs = ['actors', 'apim-admin', 'uuids']

    def get_actions(self):
        return ['deploy', 'deploy_persistence', 'deploy_services', 'update_services', 'new_tenant']

    def deploy_persistence(self, action):
        return action in ['deploy', 'deploy_persistence']

    def run_migrations(self, action):
        return self.deploy_persistence(action) or action in ['new_tenant']

    def update_docker(self, action):
        if action in ['deploy', 'deploy_services', 'new_tenant']:
            return True
        # for update actions, we shouldn't need to update the docker version.
        return False

    def check_oauth_db_role(self, server):
        """Checks for the existence of an oauth database role and derives the corresponding IP property from that.
        """
        # first, check for the all_agave_dbs role; this role defines all :
        if 'all_oauth_dbs' in server.get('roles'):
            for db in Oauth.DBs:
                if not self.args.get('oauth_{}_host'.format(db[0])):
                    self.args['oauth_{}_host'.format(db[0])] = server['ssh_host']
        # now, check individual db roles:
        for db in Oauth.DBs:
            if 'oauth_{}'.format(db[0]) in server.get('roles'):
                self.args['oauth_{}_host'.format(db[0])] = server['ssh_host']

    def audit_and_default_oauth_db_attrs(self):
        """Basic audits for database attributes.
        """
        # required fields, all DBs
        for db in Oauth.DBs:
            # host attr is required
            attr = 'oauth_{}_host'.format(db[0])
            self.configs[attr] = self.args.get(attr)
            if not self.configs[attr]:
                raise Error("Error: an {} attribute with value the IP/domain of a {} "
                            "instance is required.".format(attr, db[1]))
            self.configs['oauth_{}_type'.format(db[0])] = 'oauth'
            self.configs['{}_type'.format(db[0])] = 'oauth'

        # LDAP --
        # fields required unless using the built-in cic ldap
        cic_ldap_attrs = ['oauth_ldap_port', 'oauth_ldap_protocol', 'oauth_ldap_bind_dn',
                          'oauth_ldap_base_search_dn', 'oauth_ldap_bind_password']
        for attr in cic_ldap_attrs:
            if self.args.get(attr):
                self.configs[attr] = self.args[attr]
            else:
                if self.args.get('use_cic_ldap'):
                    if attr == 'oauth_ldap_base_search_dn':
                        # when using cic ldap, the base search dn has format:
                        self.configs[attr] = 'ou=tenant{},{}'.format(self.args['tenant'],
                                                                     defaults.oauth_ldap_base_search_dn)
                    else:
                        self.configs[attr] = getattr(defaults, attr)
                else:
                    raise Error("Error: {} is required unless using the cic ldap.".format(attr))
        self.configs['oauth_ldap_full_connection_uri'] = '{}://{}:{}'.format(self.configs['oauth_ldap_protocol'],
                                                                             self.configs['oauth_ldap_host'],
                                                                             self.configs['oauth_ldap_port']
                                                                          )
        # optional fields --
        optional_ldap_attrs = ['oauth_ldap_max_user_name_list_length', 'oauth_ldap_max_role_name_list_length',
                               'oauth_ldap_member_of', 'oauth_ldap_group_search_base', 'oauth_ldap_user_search_filter']
        for attr in optional_ldap_attrs:
            self.configs[attr] = self.args.get(attr, getattr(defaults, attr))

        # MySQL --
        self.configs['oauth_mysql_port'] = self.args.get('oauth_mysql_port')
        if not self.configs['oauth_mysql_port']:
            self.configs['oauth_mysql_port'] = defaults.oauth_mysql_port
            print("Warning: did not find a configuration for "
                  "oauth_mysql_port - using the default: {}!".format(self.configs['oauth_mysql_port']))

        self.configs['oauth_mysql_user'] = self.args.get('oauth_mysql_user')
        if not self.configs['oauth_mysql_user']:
            self.configs['oauth_mysql_user'] = defaults.oauth_mysql_user
            print("Warning: did not find a configuration for "
                  "oauth_mysql_user - using the default: {}!".format(self.configs['oauth_mysql_user']))
        self.configs['oauth_mysql_password'] = self.args.get('oauth_mysql_password')
        if not self.configs['oauth_mysql_password']:
            self.configs['oauth_mysql_password'] = defaults.oauth_mysql_password
            print("Warning: did not find a configuration for "
                  "oauth_mysql_password - using the default: {}! "
                  "THIS IS NOT SECURE.".format(self.configs['oauth_mysql_password']))
        self.configs['oauth_mysql_root_password'] = self.args.get('oauth_mysql_root_password')
        if not self.configs['oauth_mysql_root_password']:
            self.configs['oauth_mysql_root_password'] = defaults.oauth_mysql_root_password
            print("Warning: did not find a configuration for "
                  "oauth_mysql_root_password - using the default: {}! "
                  "THIS IS NOT SECURE.".format(self.configs['oauth_mysql_root_password']))

        # finally, derive the generic attributes for the persistence playbook
        self.configs['mysql_root_user'] = self.args.get('mysql_root_user', defaults.mysql_root_user)
        self.configs['mysql_host_port'] = self.args.get('oauth_mysql_port', defaults.mysql_host_port)
        self.configs['mysql_login_host'] = self.args.get('mysql_login_host', defaults.mysql_login_host)
        self.configs['mysql_version'] = self.args.get('mysql_version', defaults.mysql_version)

        self.configs['ldap_host_port'] = self.configs['oauth_ldap_port']

    def audit_tenant_properties(self):
        """
        Audit basic fields related to the tenant.
        :return:
        """
        # tenant
        self.configs['tenant'] = self.args.get('tenant')
        if not self.configs['tenant']:
            raise Error("Error: tenant is a required field.")

        # base_url
        self.configs['base_url'] = self.args.get('base_url')
        if not self.configs['base_url']:
            raise Error("Error: base_url is a required field.")
        if 'https' in self.args['base_url'] or '/' in self.args['base_url']:
            raise Error("Error: base_url should only contain a domain, not `https` or "
                        "any '/' characters.")

        # apim_admin_pass
        # todo - the apim_admin_pass can only be manually changed from the initial value because
        # it requires a change directly to the database.
        self.configs['apim_admin_pass'] = self.args.get('apim_admin_pass')
        if self.args.get('apim_admin_pass'):
            print('Warning: changing the default apim_admin_pass will requires manual changes in apim.')
        else:
            self.configs['apim_admin_pass'] = defaults.apim_admin_pass

        # use_cic_ldap
        if 'use_cic_ldap' not in self.args.keys() or not type(self.args.get('use_cic_ldap')) == bool:
            raise Error("Error: use_cic_ldap must be specified True or False.")
        self.configs['use_cic_ldap'] = self.args['use_cic_ldap']

        # jwt_header
        self.configs['jwt_header'] = self.args.get('jwt_header',
                                                   defaults.jwt_header.format(self.configs['tenant']).upper())

        # tenant_admin_role
        self.configs['tenant_admin_role'] = self.args.get('tenant_admin_role',
                                                          defaults.tenant_admin_role.format(self.args['tenant']))
        # deploy_admin_password_grant
        self.configs['deploy_admin_password_grant'] = self.args.get('deploy_admin_password_grant',
                                                                    defaults.deploy_admin_password_grant)

        # deploy_admin_services -- the deploy_admin_services flag is not supported in deployer; use the extra_apis
        if self.args.get('deploy_admin_services'):
            raise Error('Error: deploy_admin_services is not supported in deployer; use the extra_apis configuration.')
        self.configs['deploy_admin_services'] = False

        # extra_apis -
        if self.args.get('extra_apis'):
            for api in self.args.get('extra_apis'):
                # check that the api is valid -
                if api not in Oauth.EXTRA_APIs:
                    raise Error("Error: Unknown api given in extra_apis. "
                                "API {} is not in the list of known APIs: {}".format(api, Oauth.EXTRA_APIs))
            self.configs['extra_apis'] = self.args['extra_apis']
            self.configs['deploy_extra_apis'] = True
        else:
            self.configs['deploy_extra_apis'] = False

        # access_token_validity_time
        self.configs['access_token_validity_time'] = self.args.get('access_token_validity_time',
                                                                   defaults.access_token_validity_time)

        # deploy_custom_login_app -- custom login apps are not currently supported in deployer
        if self.args.get('deploy_custom_login_app'):
            raise Error('Error: custom login apps are not currently supported in deployer.')
        self.configs['deploy_custom_login_app'] = False

        # apim_increase_global_timeout
        self.configs['apim_increase_global_timeout'] = self.args.get('apim_increase_global_timeout',
                                                                     defaults.apim_increase_global_timeout)

        # agave-related properties
        # must explicitly state that not deploying agave
        if 'not_deploying_agave' not in self.args.keys():
            self.configs['agave_frontend_host'] = self.args.get('agave_frontend_host')
            if not self.configs['agave_frontend_host']:
                raise Error("Error: agave_frontend_host is required unless not_deploying_agave is explicitly set.")
            agave_frontend_host = self.configs['agave_frontend_host']
            if 'https' in agave_frontend_host or '/' in agave_frontend_host:
                raise Error("Error: agave_frontend_host should only contain an IP or domain, not `https` or "
                            "any '/' characters.")
            self.configs['agave_api_protocol'] = self.args.get('agave_api_protocol', defaults.agave_api_protocol)
            self.configs['agave_frontend_sandbox_host'] = self.args.get('agave_frontend_sandbox_host',
                                                                        defaults.agave_frontend_sandbox_host.format(self.args['tenant']))

        # abaco properties -- none are required
        self.configs['abaco_api_protocol'] = self.args.get('abaco_api_protocol', defaults.abaco_api_protocol)
        self.configs['abaco_frontend_base_url_or_ip'] = self.args.get('abaco_frontend_base_url_or_ip',
                                                                      defaults.abaco_frontend_base_url_or_ip)

        # profiles --
        self.configs['profiles_read_only'] = self.args.get('profiles_read_only', defaults.profiles_read_only)
        self.configs['profiles_check_jwt'] = self.args.get('profiles_check_jwt', defaults.profiles_check_jwt)
        self.configs['profiles_check_user_admin_role'] = self.args.get('profiles_check_user_admin_role',
                                                                       defaults.profiles_check_user_admin_role)
        self.configs['profiles_debug'] = self.args.get('profiles_debug', defaults.profiles_debug)
        # these are legacy configs that require special formatting -
        self.configs['hosted_id_domain_name'] = self.args.get('tenant')
        self.configs['agave_id_user_admin_role'] = defaults.agave_id_user_admin_role.format(self.configs['tenant'])
        self.configs['agave_id_app_base'] = defaults.agave_id_app_base.format(self.configs['base_url'])

        # clients --
        self.configs['oauth_clients_additional_apis'] = self.args.get('oauth_clients_additional_apis')
        if self.configs['oauth_clients_additional_apis']:
            # oauth_clients_additional_apis should be a list of dictionaries.
            try:
                for idx, api in enumerate(self.configs['oauth_clients_additional_apis']):
                    # check for required fields:
                    if 'version' not in api.keys():
                        raise Error("API in oauth_clients_additional_apis missing required key, `version`. "
                                    "API: {}".format(api))
                    self.configs['oauth_clients_additional_apis'][idx]['version'].encode('utf-8')
                    if 'name' not in api.keys():
                        raise Error("API in oauth_clients_additional_apis missing required key, `name`. "
                                    "API: {}".format(api))
                    # self.configs['oauth_clients_additional_apis'][idx]['name'].encode('utf-8')
                    if 'provider' not in api.keys():
                        raise Error("API in oauth_clients_additional_apis missing required key, `provider`. "
                                    "API: {}".format(api))
                    # self.configs['oauth_clients_additional_apis'][idx]['provider'].encode('utf-8')
            except Exception as e:
                raise Error("Error: Unable to process the oauth_clients_additional_apis attribute. "
                            "Exception: {}".format(e))

        else:
            self.configs.pop('oauth_clients_additional_apis')

        # finally, derive additional legacy config values using defaults:
        for legacy_config, new_config in defaults.oauth_legacy_configs.items():
            # if we have not already defined the new_config, pull the default value for it from the
            # defaults module.
            if new_config not in self.configs.keys():
                self.configs[legacy_config] = getattr(defaults, new_config)
            else:
                self.configs[legacy_config] = self.configs[new_config]

        # SSL/proxy --
        # these first properties deal with the cert content itself;
        # self-signed certs will be supplied if they are not provided as configs:
        self.configs['deploy_oauth_self_signed_cert'] = False
        self.configs['oauth_cert'] = self.args.get('oauth_cert')
        self.configs['oauth_key'] = self.args.get('oauth_key')
        self.configs['oauth_ca_cert'] = self.args.get('oauth_ca_cert')
        if not self.configs['oauth_cert'] and not self.configs['oauth_key']:
            print("WARNING: no oauth_cert/oauth_key provided; supplying a self-signed certificate.")
            self.configs['oauth_cert'] = defaults.oauth_cert
            self.configs['oauth_key'] = defaults.oauth_key
            self.configs['deploy_oauth_self_signed_cert'] = True
        # if providing cert files, all files are required
        else:
            if not self.configs['oauth_ca_cert']:
                raise Error("ERROR: oauth_ca_cert is requried when deploying operator-provided certificates.")
            if not self.configs['oauth_cert']:
                raise Error("ERROR: oauth_cert is requried when deploying operator-provided certificates.")
            if not self.configs['oauth_key']:
                raise Error("ERROR: oauth_key is requried when deploying operator-provided certificates.")
        # these fields are part of the legacy operation of mounting the cert files and refer to the host paths
        # where the certs will be deployed; these should not be changed by the operator.
        self.configs['oauth_proxy_cert_file'] = defaults.oauth_proxy_cert_file.format(self.args['tenant'])
        self.configs['oauth_proxy_cert_key_file'] = defaults.oauth_proxy_cert_key_file.format(self.args['tenant'])
        # these configs are needed for the proxy image itself - just need the file name itself
        self.configs['cert_file'] = self.configs['oauth_proxy_cert_file'].split('/')[-1]
        self.configs['cert_key_file'] = self.configs['oauth_proxy_cert_key_file'].split('/')[-1]
        # the ca-cert file is not used when deploying self-signed certs
        if self.configs['deploy_oauth_self_signed_cert']:
            self.configs['oauth_proxy_ca_cert_file'] = None
        else:
            self.configs['oauth_proxy_ca_cert_file'] = defaults.oauth_proxy_ca_cert_file.format(self.args['tenant'])
            self.configs['ssl_ca_cert_file'] = self.configs['oauth_proxy_ca_cert_file'].split('/')[-1]

    def get_image(self, service):
        """
        Determine Docker images to use for auth services.
        :return:
        """
        reg = ''
        if self.args.get('oauth_docker_registry'):
            reg = self.args.get('oauth_docker_registry')
        org = defaults.oauth_docker_registry_org
        if self.args.get('agave_docker_registry_org'):
            org = self.args.get('agave_docker_registry_org')
        image_name = None
        for s in defaults.services:
            if s['service'] == service:
                image_name = s['image']
                break
        # the oauth_lb service is not an A/B service:
        if service == 'oauth-lb':
            image_name = defaults.oauth_lb_image
        # oauth_migrations is not an A/B service:
        if service == 'oauth_migrations':
            image_name = defaults.oauth_migrations_image
        if not image_name:
            raise Error('Error: Unable to look up the default image name for service: {}'.format(service))
        version = defaults.oauth_version
        if self.args.get('oauth_version'):
            version = self.args.get('oauth_version')
        else:
            self.args['oauth_version'] = version
        if reg:
            return '{}/{}/{}:{}'.format(reg, org, image_name, version)
        else:
            return '{}/{}:{}'.format(org, image_name, version)

    def get_service_images(self):
        """
        Get all service images
        """
        self.configs['oauth_lb_image'] = self.get_image('oauth-lb')
        self.configs['proxy_image'] = self.get_image('proxy')
        self.configs['apim_image'] = self.get_image('apim')
        self.configs['profiles_image'] = self.get_image('profiles')
        self.configs['clients_image'] = self.get_image('clients')
        self.configs['oauth_migrations_image'] = self.get_image('oauth_migrations')
        self.configs['apim_admin_image'] = self.get_image('apim_admin')

    def get_environment(self):
        """Return a dictionary representing the environment to be injecting in all oauth gateway services."""

        # for now, we return only the passwords in favor of the oauth-config.yml file which will be mounted in the
        # containers. in a future release, the environment can be leveraged so that mounts are not needed.
        return {'auth_ldap_bind_password': self.configs.get('oauth_ldap_bind_password'),
                'apim_admin_pass': self.configs.get('apim_admin_pass'),
                'mysql_tenant_user': self.configs.get('oauth_mysql_user'),
                'mysql_user': self.configs.get('oauth_mysql_user'),
                'mysql_tenant_pass': self.configs.get('oauth_mysql_password'),
                'mysql_password': self.configs.get('oauth_mysql_password'),
                'mysql_root_password': self.configs.get('oauth_mysql_root_password'),
                }

    def audit_configs(self):
        """
        Primary config audit
        :return:
        """
        #
        self.audit_and_default_oauth_db_attrs()
        self.audit_tenant_properties()
        # lastly, fill in the environment
        self.configs['service_environment'] = self.get_environment()
        self.get_service_images()
        self.configs.update(self.configs['service_environment'])

    def do_action(self, action):
        actions = self.get_actions()
        if action not in actions:
            raise Error("Error: {} is not a valid oauth action. Valid actions: {}".format(action, actions))
        if action == 'update_services':
            self.configs['remove_existing_services'] = True
        else:
            self.configs['remove_existing_services'] = False

        # check for db roles and derive properties if found
        for s in self.servers:
            self.check_oauth_db_role(s)

        # primary config audit
        try:
            self.audit_configs()
        except Exception as e:
            raise Error("Error - Unable to audit configs; Info: {}".format(e))

        # call the service_host playbook for these servers and configs
        commons = ['service_host', 'legacy_docker_daemon']
        role_map = {'oauth': ['service_host', 'legacy_docker_host'],
                    'all_oauth_dbs': ['service_host', 'legacy_docker_host', 'oauth_mysql',],
                    'oauth_ldap': ['service_host', 'legacy_docker_host'],
                    'oauth_mysql': ['service_host', 'legacy_docker_host'],
                    }
        if self.args.get('use_cic_ldap'):
            role_map['all_oauth_dbs'].append('oauth_ldap')
        self.apply_derived_roles(role_map=role_map)

        # set version of docker:
        if not self.configs.get('dont_update_docker'):
            self.configs['update_docker_version'] = True
            self.configs['docker_version'] = '17.05.0.ce-1.el7.centos'
            self.configs['update_docker_compose_version'] = True
            self.configs['docker_compose_version'] = '1.18.0'
            self.configs['update_docker_version'] = self.update_docker(action)

        # add the persistence playbook if deploying the oauth databases
        self.configs['deploy_persistence'] = False
        self.configs['run_migrations'] = False
        if self.deploy_persistence(action):
            commons.append('persistence')
            self.configs['deploy_persistence'] = True
            self.configs['run_migrations'] = True
        elif self.run_migrations(action):
            commons.append('persistence')
            self.configs['run_migrations'] = True

        # execute the common playbooks
        try:
            self.execute_common_playbooks(commons)
        except Exception as e:
            raise Error(msg="Unhandled exception executing common playbooks. Exception: {}".format(e))

        # call the actual oauth playbook
        self.execute_playbook('oauth.plbk', keep_files=self.args['keep_tempfiles'])

