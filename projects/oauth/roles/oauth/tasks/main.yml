---

- include_vars: _deployer_configuration.yml
  when: from_deployer

- name: ensure compose directories are present
  file: path=/home/apim/{{ tenant }} state=directory

- name: compile config template
  template: src=oauth-config.yml.j2 dest=/home/apim/{{ tenant }}/oauth-config.yml

- name: compile oauth cert
  template: src=oauth.cert.j2 dest=/home/apim/{{ tenant }}/oauth.cert

- name: compile oauth ca cert
  template: src=oauth_ca.cert.j2 dest=/home/apim/{{ tenant }}/oauth_ca.cert

- name: compile oauth key
  template: src=oauth.key.j2 dest=/home/apim/{{ tenant }}/oauth.key

- name: compile lb template
  template: src=lb.yml.j2 dest=/home/apim/{{ tenant }}/lb.yml

#- debug: msg="Value of environment-- {{ service_environment }}"

- name: compile service A template
  template: src=servicesA.yml.j2 dest=/home/apim/{{ tenant }}/servicesA.yml

- name: compile service B template
  template: src=servicesB.yml.j2 dest=/home/apim/{{ tenant }}/servicesB.yml

- name: pull new versions of the images
  command: docker-compose -f /home/apim/{{ tenant }}/servicesA.yml pull

- name: start auth LB
  command: docker-compose -f /home/apim/{{ tenant }}/lb.yml up -d
  environment:
    COMPOSE_HTTP_TIMEOUT: 300


# =========================================
# Auth Services Rolling Deployment - Start
# =========================================

- name: check if A_deployed
  stat: path=/home/apim/{{ tenant }}/A_deployed
  register: A

- name: msg
  debug: msg="A containers are not deployed (A_deployed path doesn't exist)"
  when: A.stat.exists == False

- name: check if B_deployed
  stat: path=/home/apim/{{ tenant }}/B_deployed
  register: B

- name: msg
  debug: msg="B containers are not deployed (B_deployed path doesn't exist)"
  when: B.stat.exists == False

# -------------
# A deployment
# -------------

# if A is not deployed, always deploy it
- name: start A services
  command: docker-compose -p a -f /home/apim/{{ tenant }}/servicesA.yml up -d
  environment:
    COMPOSE_HTTP_TIMEOUT: 300
  when: A.stat.exists == false

- name: ensure the A_deployed file exists
  file: path=/home/apim/{{ tenant }}/A_deployed state=touch
  when: A.stat.exists == false

- pause: minutes=2 prompt="Pausing while A containers start up before removing B containers."
  when: A.stat.exists == false and B.stat.exists

# stop & remove B containers when they are defined
- name: stop and remove B containers
  command: docker-compose -p b -f /home/apim/{{ tenant }}/servicesB.yml down
  environment:
      COMPOSE_HTTP_TIMEOUT: 300
  when: B.stat.exists

- pause: minutes=1 prompt="Pausing while B containers are removed."
  when: A.stat.exists == false and B.stat.exists

# restart haproxy to catch new A stack
- name: restart haproxy
  command: docker exec -i {{ tenant }}_oauth-lb_1 service haproxy restart
  when: B.stat.exists

- name: remove the B_deployed file
  file: path=/home/apim/{{ tenant }}/B_deployed state=absent
  when: B.stat.exists

# -------------
# B deployment
# -------------

# deploy B when A is defined
- name: start B services
  command: docker-compose -p b -f /home/apim/{{ tenant }}/servicesB.yml up -d
  when: A.stat.exists

- name: ensure the B_deployed file exists
  file: path=/home/apim/{{ tenant }}/B_deployed state=touch
  when: A.stat.exists

- pause: minutes=2 prompt="Pausing while B containers start up before removing A containers."
  when: A.stat.exists and B.stat.exists == false

# stop & remove A containers when they are defined
- name: stop A containers
  command: docker-compose -p a -f /home/apim/{{ tenant }}/servicesA.yml down
  environment:
      COMPOSE_HTTP_TIMEOUT: 300
  when: A.stat.exists

- pause: minutes=1 prompt="Pausing while A containers are removed."
  when: A.stat.exists and B.stat.exists == false

# restart haproxy to catch new B stack
- name: restart haproxy
  command: docker exec -i {{ tenant }}_oauth-lb_1 service haproxy restart
  when: A.stat.exists

- name: remove the A_deployed file
  file: path=/home/apim/{{ tenant }}/A_deployed state=absent
  when: A.stat.exists


# ===========
# Extra_apis
# ===========

- include_role:
    name: extra_apis
  when: deploy_extra_apis
