# Default set of Agave frontend services:

# *** WARNING - the names of services in the following lists much match the names in the service_specific_ports
#               and service_specific_volumes lists below!
FRONTEND_SERVICES = [{'service':'apps'},
                     {'service':'files'},
                     {'service':'jobs'},
                     # todo -
                     # new aloe frontend jobs just to handle POSTs to create jobs when ready
                     # {'service':'jobs-aloe'},
                     {'service':'monitors'},
                     {'service':'metadata'},
                     {'service':'notifications'},
                     {'service':'postits'},
                     {'service':'systems'},
                     {'service':'tenants'},
                     {'service':'uuids'}
                     ]
for s in FRONTEND_SERVICES:
    s['name'] = s['service']
    s['image'] = '{}-api'.format(s['service'])

# handle proxy separately, because it is... special:
FRONTEND_SERVICES.append({'service':'proxy',
                          'name': 'proxy',
                          # todo - need agave team to make the proxy image versioning consistent with the other
                          # services.
                          'image': 'httpd:2.4.29'
                          })

# Default set of worker services:
WORKER_SERVICES = [{'service': 'files-worker',
                    'image': 'files-api',
                    },
                   {'service': 'jobs-worker',
                    'image': 'jobs-api',
                    },
                   ]
for s in WORKER_SERVICES:
    s['name'] = s['service']

# Docker Images
agave_docker_registry_org = 'tacc'
agave_version = '2.2.21'

# the base URL used to construct the default routing rules in the proxy
BASE_PROXY_URL = '172.17.0.1'

# the public ports used to to construct the default port mappings and proxy routing rules
APPS_PUBLIC_PORT = '8083'
FILES_PUBLIC_PORT = '8082'
JOBS_PUBLIC_PORT = '8081'
METADATA_PUBLIC_PORT = '8087'
MONITORS_PUBLIC_PORT = '8085'
NOTIFICATIONS_PUBLIC_PORT = '8088'
POSTITS_PUBLIC_PORT = '8090'
SYSTEMS_PUBLIC_PORT = '8084'
TENANTS_PUBLIC_PORT = '8092'
UUIDS_PUBLIC_PORT = '8078'

# Attributes for all Agave services - these are "user friendly" names (because they can be provided
# by the user). Translation into various properties defined in mappings below.
agave_attrs = {
    # attributes common to all services
    'common': {
        'enable_remote_debug': 1,
        'agave_iplant_slave_mode': 'false',
        'agave_log_service' : 'http://172.17.0.1/logging',
        'tenant_id': '',
        'hostname' : 'agave.core',
        'agave_java_mem_limit' : '1024m',

        # docker
        'image': None,
        'volumes': None,
        'ports': None,

        'zombie_cleanup_interval' : '5',
        'zombie_cleanup_enabled' : 'true',
        'zombie_task_lifetime' : '15',
        'zombie_cleanup_batch_size' : '100',
        'drain_all_queues': 'false',

        'agave_allow_relay_transfers' : 'true',
        'agave_max_relay_transfer_size' : '5',

        'agave_max_page_size' : '250',
        'agave_default_page_size' : '99',

        # mysql
        'mysql_type': '',
        'agave_mysql_host': '',
        'agave_mysql_port': '',
        'agave_mysql_user' : 'agaveapi',
        'agave_mysql_password' : 'rmQ139zV4',
        'agave_mysql_root_password': 'rmQ139zV4',
        'agave_mysql_database': 'agaveapi',
        'agave_mysql_version': '5.6',

        # mongo db
        'mongo_type': '',
        'agave_mongo_host': '',
        'agave_mongo_user' : 'agaveapi',
        'agave_mongo_password' : 'rmQ139zV4',
        'agave_mongo_port' : '',
        'agave_mongo_version': '3.4',
        'mongo_admin_user': 'adminuser',
        'mongo_admin_password': 'rmQ139zV4',

        # notifications
        'agave_notifications_queue' : 'sandbox.notifications.queue',
        'agave_notifications_topic' : 'sandbox.notifications.topic',
        'agave_notifications_db_scheme': 'notifications',
        'agave_notifications_db_host' : '',
        'agave_notifications_db_user' : '',
        'agave_notifications_db_port' : '',
        'agave_catalina_opts': '"-Duser.timezone=America/Chicago -Djsse.enableCBCProtection=false -Djava.awt.headless=true -Dfile.encoding=UTF-8 -server -Xms1152m -Xmx1152m -XX:+DisableExplicitGC -Djava.security.egd=file:/dev/./urandom"',

        # beanstalk
        'beanstalk_type': '',
        'agave_beanstalk_host': '',
        'agave_beanstalk_port': '11300',

        # rabbitmq
        'rabbitmq_type': '',
        'agave_rabbitmq_version': '3.7.7',
        'agave_rabbitmq_port': '5671',
        'agave_rabbitmq_port2': '5672',
        'agave_rabbitmq_port3': '15671',
        'agave_rabbitmq_port4': '15672',

        # mail
        'agave_smtps_from_name': 'agave',
        'agave_smtps_host': 'smtp.sendgrid.net',
        'agave_smtps_auth': True,
        'agave_smtps_port': 587,
        'agave_smtps_user': 'agavedev',
        'agave_smtps_password': 'rmQ139zV4',
    },

    # service-specific attributes
    'files-worker': {
        'files_staging_tasks': 10,
        'files_transform_tasks': 5,
        'files_globus_tcp_port_range': '50301,50310',
    },

    'files': {
        'files_staging_tasks': 10,
        'files_transform_tasks': 5,
        'files_globus_tcp_port_range': '50321,50330',
    },

    'jobs-worker': {
        'jobs_staging_tasks': 10,
        'jobs_submission_tasks': 10,
        'jobs_monitoring_tasks': 10,
        'jobs_archiving_tasks': 10,
        'jobs_globus_tcp_port_range': '50078,50087',
    },

    'proxy': {
        'apps_proxy_url': '{}:{}'.format(BASE_PROXY_URL, APPS_PUBLIC_PORT),
        'files_proxy_url': '{}:{}'.format(BASE_PROXY_URL, FILES_PUBLIC_PORT),
        'jobs_proxy_url': '{}:{}'.format(BASE_PROXY_URL, JOBS_PUBLIC_PORT),
        'monitors_proxy_url': '{}:{}'.format(BASE_PROXY_URL, MONITORS_PUBLIC_PORT),
        'metadata_proxy_url': '{}:{}'.format(BASE_PROXY_URL, METADATA_PUBLIC_PORT),
        'notifications_proxy_url': '{}:{}'.format(BASE_PROXY_URL, NOTIFICATIONS_PUBLIC_PORT),
        'postits_proxy_url': '{}:{}'.format(BASE_PROXY_URL, POSTITS_PUBLIC_PORT),
        'systems_proxy_url': '{}:{}'.format(BASE_PROXY_URL, SYSTEMS_PUBLIC_PORT),
        'tenants_proxy_url': '{}:{}'.format(BASE_PROXY_URL, TENANTS_PUBLIC_PORT),
        'uuids_proxy_url': '{}:{}'.format(BASE_PROXY_URL, UUIDS_PUBLIC_PORT),

    }
}

# ------------
# ENVIRONMENT
# ------------

# Dictionary of translations from properties names (provided by the user) to environment variable names
# recognized by the actual Agave services. The key in the dictionary is the property named a user can specify, and the
# value is the actual environment variable name recognized by Agave.
#
# Note: all possible environment variables (even service, specific ones) must be defined as keys in this dictionary
# and must have a corresponding property name that at least is defined as a default value.
agave_environment_translations = {
     'agave_allow_relay_transfers': 'IPLANT_ALLOW_RELAY_TRANSFERS',
     'agave_catalina_opts': 'CATALINA_OPTS',
     'agave_monitor_min_check_interval': 'IPLANT_MIN_MONITOR_REPEAT_INTERVAL',

     'agave_iplant_internal_account_service': 'IPLANT_INTERNAL_ACCOUNT_SERVICE',
     'agave_iplant_internal_account_service_key': 'IPLANT_INTERNAL_ACCOUNT_SERVICE_KEY',
     'agave_iplant_internal_account_service_secret': 'IPLANT_INTERNAL_ACCOUNT_SERVICE_SECRET',
     'agave_iplant_slave_mode': 'IPLANT_SLAVE_MODE',

     'agave_log_service': 'IPLANT_LOG_SERVICE',
     'agave_max_page_size': 'IPLANT_MAX_PAGE_SIZE',
     'agave_default_page_size': 'IPLANT_DEFAULT_PAGE_SIZE',
     'agave_max_relay_transfer_size': 'IPLANT_MAX_RELAY_TRANSFER_SIZE',

     'agave_messaging_host': 'IPLANT_MESSAGING_HOST',
     'agave_messaging_port': 'IPLANT_MESSAGING_PORT',

     'agave_mongo_host': 'IPLANT_METADATA_DB_HOST',
     'agave_mongo_password': 'IPLANT_METADATA_DB_PWD',
     'agave_mongo_port': 'IPLANT_METADATA_DB_PORT',
     'agave_mongo_user': 'IPLANT_METADATA_DB_USER',

     'agave_mysql_database': 'MYSQL_DATABASE',
     'agave_mysql_host': 'MYSQL_HOST',
     'agave_mysql_password': 'MYSQL_PASSWORD',
     'agave_mysql_port': 'MYSQL_PORT',
     'agave_mysql_user': 'MYSQL_USERNAME',

     'agave_notifications_db_host': 'IPLANT_NOTIFICATION_FAILED_DB_HOST',
     'agave_notifications_db_password': 'IPLANT_NOTIFICATION_FAILED_DB_PWD',
     'agave_notifications_db_port': 'IPLANT_NOTIFICATION_FAILED_DB_PORT',
     'agave_notifications_db_scheme': 'IPLANT_NOTIFICATION_FAILED_DB_SCHEME',
     'agave_notifications_db_user': 'IPLANT_NOTIFICATION_FAILED_DB_USER',
     'agave_notifications_queue': 'IPLANT_NOTIFICATION_SERVICE_QUEUE',
     'agave_notifications_topic': 'IPLANT_NOTIFICATION_SERVICE_TOPIC',

     'agave_smtps_auth': 'MAIL_SMTPS_AUTH',
     'agave_smtps_from_address': 'MAIL_SMTPS_FROM_ADDRESS',
     'agave_smtps_from_name': 'MAIL_SMTPS_FROM_NAME',
     'agave_smtps_host': 'MAIL_SMTPS_HOST',
     'agave_smtps_password': 'MAIL_SMTPS_PASSWD',
     'agave_smtps_port': 'MAIL_SMTPS_PORT',
     'agave_smtps_provider': 'MAIL_SMTPS_PROVIDER',
     'agave_smtps_user': 'MAIL_SMTPS_USER',

     'drain_all_queues': 'IPLANT_DRAIN_ALL_QUEUES',
     'enable_remote_debug': 'ENABLE_REMOTE_DEBUG',

     'files_globus_tcp_port_range': 'GLOBUS_TCP_PORT_RANGE',
     'files_staging_tasks': 'IPLANT_MAX_STAGING_TASKS',
     'files_transform_tasks': 'IPLANT_MAX_TRANSFORM_TASKS',

     'jobs_globus_tcp_port_range': 'GLOBUS_TCP_PORT_RANGE',

     'tenant_id': 'IPLANT_DEDICATED_TENANT_ID',

     'zombie_cleanup_batch_size': 'IPLANT_ZOMBIE_CLEANUP_BATCH_SIZE',
     'zombie_cleanup_enabled': 'IPLANT_ENABLE_ZOMBIE_CLEANUP',
     'zombie_cleanup_interval': 'IPLANT_ZOMBIE_CLEANUP_INTERVAL',
     'zombie_task_lifetime': 'IPLANT_ZOMBIE_TASK_LIFETIME'
}


# -----
# PORTS
# -----

# Dictionary of port mappings for all Agave services
common_ports = {

}


# Dictionary of service-specific port mappings; the keys in this dictionary *must match* the service
# names in the AGAVE_WORKERS and AGAVE_FRONTENDS dictionaries at the top.
service_specific_ports = {
    'apps': {
        APPS_PUBLIC_PORT: '80',
        '8446': '443',
        '50901-50910': '50901-50910',
        '52922': '52911',
    },
    'files': {
        FILES_PUBLIC_PORT: '80',
        '8445': '443',
        '50321-50330': '50321-50330',
        '30301-30310': '30301-30310',
    },
    'files-worker': {
        '50301-50310': '50301-50310',
        '52921': '52911',
    },
    'jobs': {
        JOBS_PUBLIC_PORT: '80',
        '8444': '443',
        '50007-50017': '50007-50017',
    },
    'jobs-worker': {
        '9081': '80',
        '9444': '443',
        '50078-50087': '50078-50087',
    },
    'metadata': {
        METADATA_PUBLIC_PORT: '80',
        '8450': '443',
        '52926': '52911',
    },
    'monitors': {
        MONITORS_PUBLIC_PORT: '80',
        '8448': '443',
        '52924': '52911',
    },
    'notifications': {
        NOTIFICATIONS_PUBLIC_PORT: '80',
        '8451': '443',
        '52927': '52911',
    },
    'postits': {
        POSTITS_PUBLIC_PORT: '80',
        '8453': '443',
    },
    'proxy': {
        '80': '80',
        '443': '443',
    },
    'systems': {
        SYSTEMS_PUBLIC_PORT: '80',
        '8447': '443',
        '52923': '52911',
    },
    'tenants': {
        TENANTS_PUBLIC_PORT: '80',
        '8455': '443',
    },
    'uuids': {
        UUIDS_PUBLIC_PORT: '80',
        '8459': '443',
        '52930': '52911',
    },
}


# --------
# VOLUMES
# --------

# Dictionary of volume mappings for all Agave services
common_volumes = {
}

# Dictionary of service-specific volumes; the keys in this dictionary *must match* the service
# names in the AGAVE_WORKERS and AGAVE_FRONTENDS dictionaries at the top.
service_specific_volumes = {
    'apps': {
        '/var/log/splunk/apps': '/opt/tomcat/logs',
        './scratch/apps': '/scratch',
    },
    'files': {
        '/var/log/splunk/files': '/opt/tomcat/logs',
        './scratch/files': '/scratch',
    },
    'files-worker': {
        '/var/log/splunk/files': '/opt/tomcat/logs',
        './scratch/files': '/scratch',
    },
    'jobs': {
        '/var/log/splunk/jobs': '/opt/tomcat/logs',
        './scratch/jobs': '/scratch',
    },
    'jobs-worker': {
        '/var/log/splunk/jobs': '/opt/tomcat/logs',
        './scratch/jobs': '/scratch',
    },
    'metadata': {
        '/var/log/splunk/meta': '/opt/tomcat/logs',
        './scratch/meta': '/scratch',
    },
    'monitors': {
        '/var/log/splunk/monitors': '/opt/tomcat/logs',
        './scratch/monitors': '/scratch',
    },
    'notifications': {
        '/var/log/splunk/notifications': '/opt/tomcat/logs',
        './scratch/notifications': '/scratch',
    },
    'postits': {
        '/var/log/splunk/postits': '/opt/tomcat/logs',
        './scratch/postits': '/scratch',
    },
    'proxy': {
        '/var/log/splunk/proxy': '/var/log/apache2',
        './httpd.conf': '/usr/local/apache2/conf/httpd.conf:ro',
    },
    'systems': {
        '/var/log/splunk/systems': '/opt/tomcat/logs',
        './scratch/systems': '/scratch',
    },
    'tenants': {
        '/var/log/splunk/tenants': '/opt/tomcat/logs',
        './scratch/tenants': '/scratch',
    },
    'uuids': {
        '/var/log/splunk/uuids': '/opt/tomcat/logs',
        './scratch/uuids': '/scratch',
    },
}


# =====================
# Databases/Persistence
# =====================

agave_db_attrs = {
    'mysql': {
        'port': {3301: 3306},
        'user': 'agaveapi',
        'password': 'rmQ139zV4'
    },
    'mongo': {
        'port': {27017: 27017},
        'user': 'agaveapi',
        'password': 'rmQ139zV4'
    },
    'beanstalk': {
        'port': {11300: 11300},
        'user': 'agaveapi',
        'password': 'rmQ139zV4'
    },
    'rabbitmq': {
        'port': {5672: 5672},
        'user': 'agaveapi',
        'password': 'rmQ139zV4'
    },

}