import copy
from util.project import Project, Error
from . import defaults


class Agave(Project):

    """
    Project class to handle Agave deployment actions.

    The high-level approach is:
      A. Determine the set of Agave services that will be running on each server.
      B. Look for user-supplied configuration coming from multiple sources (see below) and resolve it with default
         values (supplied in defaults.py) to a set of configs for each service on each server
      C. For each config for each service, translate the config name to something Agave recognizes (such as an
         environment variable with a known name).

    User-supplied configuration could reside in three places:
      1. Individual configs could live on the (flattened) configs object, available in the args property.
      2. Properties could be defined for a specific host within the servers object.
      3. Each server in the servers list could specify two properties: agave_frontends and agave_workers. Each
         of these attributes is a list of dictionaries of specific Agave services that could also specify specific
         configs for the given service.

    Additionally, default values are supplied in the defaults.py module.

    Resolution strategy is as follows:
      1. check to see if the config attribute is defined on the service object itself.
      2. if not, check to see if it is defined on the server object.
      3. if not, check to see if it is defined in the args object (this should be properly flattened)
      4. last of all, use the default.

    """

    DBs = [('mysql', 'MySQL'),
           ('mongo', 'MongoDB'),
           ('beanstalk', 'Beanstalk'),
           ('rabbitmq', 'RabbitMQ')]

    def get_actions(self):
        return ['deploy', 'deploy_persistence', 'deploy_services', 'update_services']

    def deploy_persistence(self, action):
        return action in ['deploy', 'deploy_persistence']

    def update_docker(self, action):
        return action in ['deploy', 'deploy_persistence']

    def persistence_property_lookup(self, property, default=None):
        """
        Look up a persistence property, such as the agave_mysql_port, from 
        :return: 
        """
        if self.args.get(property):
            return self.args.get(property)

        # special handling for agave_migrations_image since it is derived from docker registry, agave_version, etc.
        if property == 'agave_migrations_image':
            return self.get_image('migrations')
        return defaults.agave_attrs['common'].get(property, default)

    def derive_persistence_attrs(self):
        """
        Because the persistence project is a common project, we need to derive a set of "generic" attributes from the
        Agave-specific attribute names; e.g.,
        
        mysql_type -> "agave" (the type of mysql server that will be run)
        mysql_host_port -> agave_mysql_port
        
        :return: 
        """
        # common attrs
        for db in Agave.DBs:
            for s in self.servers:
                if 'agave_{}'.format(db[0]) or 'all_agave_dbs' in s.get('roles'):
                    # the types distinguish different databases being used by different projects
                    s['{}_type'.format(db[0])] = 'agave'
                    s['{}_host'.format(db[0])] = self.persistence_property_lookup('agave_{}_host'.format(db[0]))
                    s['{}_host_port'.format(db[0])] = self.persistence_property_lookup('agave_{}_port'.format(db[0]))
                    s['{}_user'.format(db[0])] = self.persistence_property_lookup('agave_{}_user'.format(db[0]))
                    s['{}_version'.format(db[0])] = self.persistence_property_lookup('agave_{}_version'.format(db[0]))

                # mongo-specific attrs:
                if 'agave_mongo' or 'all_agave_dbs' in s.get('roles'):
                    s['mongo_password'] = self.persistence_property_lookup('agave_mongo_password')
                    s['mongo_admin_user'] = self.persistence_property_lookup('mongo_admin_user')
                    s['mongo_admin_password'] = self.persistence_property_lookup('mongo_admin_password')

                # mysql-specific attrs:
                if 'agave_mysql' or 'all_agave_dbs' in s.get('roles'):
                    s['mysql_database'] = 'agaveapi'
                    s['mysql_password'] = self.persistence_property_lookup('agave_mysql_password')
                    s['mysql_root_user'] = self.persistence_property_lookup('agave_mysql_root_user', 'root')
                    s['mysql_root_password'] = self.persistence_property_lookup('agave_mysql_root_password')
                    s['agave_migrations_image'] = self.persistence_property_lookup('agave_migrations_image')
                    # flyway migrations container needs the agave version string separately:
                    s['agave_version'] = self.args['agave_version']

                # rabbitmq
                if 'agave_rabbitmq' or 'all_agave_dbs' in s.get('roles'):
                    s['rabbitmq_port2'] = self.persistence_property_lookup('agave_rabbitmq_port2')
                    s['rabbitmq_port3'] = self.persistence_property_lookup('agave_rabbitmq_port3')
                    s['rabbitmq_port4'] = self.persistence_property_lookup('agave_rabbitmq_port4')

    def get_all_properties_for_service(self, service):
        """
        Returns the list of all possible properties for a given service.
        :param service: str - a valid Agave service
        :return:
        """
        # first, get service-specific attributes; the service key may not exist in agave_attrs:
        try:
            l1 = list(defaults.agave_attrs[service].keys())
        except:
            l1 = []
        # create a list from the common keys and extend by l1
        result = list(defaults.agave_attrs['common'].keys())
        result.extend(l1)
        return result

    def get_default(self, service, property):
        """
        Returns the default value for a property and a given service.
        :param service: str - a valid Agave service (e.g., "apps")
        :param property: str - a property name (e.g., "agave_java_mem_limit")
        :return:
        """
        # first check if the property is defined for the specific service:
        try:
            return defaults.agave_attrs[service][property]
        except KeyError:
            # if not there, return from common:
            try:
                return defaults.agave_attrs['common'][property]
            except KeyError:
                raise Error('Error: Did not find a default '
                            'value for property {}, service {}'.format(property, service))

    def get_volumes_for_service(self, service):
        """
        Returns the default volumes for `service`, a string representing a valid Agave service, by overlaying
        and service-specific default volumes over the common default volumes.
        :param service: string - valid agave service.
        :return:
        """
        d = copy.deepcopy(defaults.common_volumes)
        d.update(defaults.service_specific_volumes.get(service))
        return d


    def get_ports_for_service(self, service):
        """
        Returns the default ports for `service`, a string representing a valid Agave service, by overlaying
        and service-specific default ports over the common default ports.
        :param service: string - valid agave service.
        :return:
        """
        d = copy.deepcopy(defaults.common_ports)
        d.update(defaults.service_specific_ports.get(service))
        return d

    def check_agave_db_role(self, server):
        """Checks for the existence of an agave database role and derives the corresponding IP property from that.
        """
        # first, check for the all_agave_dbs role; this role defines all :
        if 'all_agave_dbs' in server.get('roles'):
            for db in Agave.DBs:
                if not self.args.get('agave_{}_host'.format(db[0])):
                    self.args['agave_{}_host'.format(db[0])] = server['ssh_host']
        # now, check individual db roles:
        for db in Agave.DBs:
            if 'agave_{}'.format(db[0]) in server.get('roles'):
                self.args['agave_{}_host'.format(db[0])] = server['ssh_host']

    def audit_and_default_agave_db_attrs(self):
        """Basic audits for database attributes.
        """
        for db in Agave.DBs:
            # host attr is required
            if not self.args.get('agave_{}_host'.format(db[0])):
                raise Error("Error: an {} with value the IP/domain of a {} "
                            "instance is required.".format(db[0], db[1]))
            # other attrs have defaults
            if not self.args.get('agave_{}_port'.format(db[0])):
                # the first (and only) key in the ports dict is the host port for the db - that's the one we want to
                # use as the default; next(iter(keys)) returns the a (the) key:
                ports_keys = defaults.agave_db_attrs.get(db[0]).get('port').keys()
                port = next(iter(ports_keys))
                print("Warning: did not find a configuration for "
                      "agave_{}_port - using the default: {}!".format(db[0], port))
                self.args['agave_{}_port'.format(db[0])] = port
            if not self.args.get('agave_{}_user'.format(db[0])):
                print("Warning: did not find a configuration for "
                      "agave_{}_user - using the default: {}!".format(db[0],
                                                                      defaults.agave_db_attrs.get(db[0]).get('user')))
                self.args['agave_{}_user'.format(db[0])] = defaults.agave_db_attrs.get(db[0]).get('user')
            if not self.args.get('agave_{}_password'.format(db[0])):
                print("Warning: did not find a configuration for "
                      "agave_{}_password - using the default: {}! "
                      "THIS IS INSECURE!".format(db[0], defaults.agave_db_attrs.get(db[0]).get('password')))
                self.args['agave_{}_password'.format(db[0])] = defaults.agave_db_attrs.get(db[0]).get('password')
            # notifications db properties are the same as mongo
            if db[0] == 'mongo':
                if not self.args.get('agave_notifications_db_host'):
                    self.args['agave_notifications_db_host'] = self.args.get('agave_mongo_host')
                if not self.args.get('agave_notifications_db_user'):
                    self.args['agave_notifications_db_user'] = self.args.get('agave_mongo_user')
                if not self.args.get('agave_notifications_db_port'):
                    self.args['agave_notifications_db_port'] = self.args.get('agave_mongo_port')

    def get_image(self, service):
        """Returns the full image name, including registry and tag, for `service`, a string with the name of the
        service.
        """
        # if the user has specified an exact image to use, simply return that:
        if self.args.get('agave_{}_image'.format(service)):
            return self.args.get('agave_{}_image'.format(service))

        # otherwise, we derive the image from a series of configs:
        # the docker registry to use
        reg = ''
        if self.args.get('agave_docker_registry'):
            reg = self.args.get('agave_docker_registry')
        org = defaults.agave_docker_registry_org
        if self.args.get('agave_docker_registry_org'):
            org = self.args.get('agave_docker_registry_org')
        name = None
        # migrations "service" is neither a frontend or worker service, so is handled separately:
        if service == 'migrations':
            # todo - migrations image has the name "agave" in it.
            name = 'agave-migrations'
        else:
            for s in defaults.FRONTEND_SERVICES + defaults.WORKER_SERVICES:
                if service == s['service']:
                    name = s['image']
        if not name:
            raise Error('Error: Unable to look up the default image name for service: {}'.format(service))
        # todo - currently, the agave proxy does not adhere to the same rules for version and docker org;
        #        therefore, we have hardcoded the correct, fully qualified image in the defaults module.
        #        this should be removed once the agave team starts publishing a proxy image with the proper versions.
        if service == 'proxy':
            return name
        version = defaults.agave_version
        if self.args.get('agave_version'):
            version = self.args.get('agave_version')
        else:
            self.args['agave_version'] = version
        if reg:
            return '{}/{}/{}:{}'.format(reg, org, name, version)
        else:
            return '{}/{}:{}'.format(org, name, version)


    def derive_service_properties(self, service_object, service_type, server):
        """
        Derive the properties for a specific service object from various sources of configuration.
        :param service_object: dict - The dictionary representing the service.
        :param service_type: str - "workers" or "frontends"; used for setting messages.
        :param server: dict - the server that the service will run on.
        :return:
        """
        if not type(service_object) == dict:
            raise Error("Error: each entry in an agave_{} config must be a dictionary.".format(service_type))
        if not service_object.get('name') or not type(service_object.get('name') == str):
            raise Error("Error: each entry in an agave_{} config must have a name property that "
                        "is a string type.".format(service_type))
        if not service_object.get('service') or not type(service_object.get('service') == str):
            raise Error("Error: each entry in an agave_{} config must have a service property that "
                        "is a string type equal to one of the recognized worker services.".format(service_type))

        if service_type == 'workers':
            services = [w.get('service') for w in defaults.WORKER_SERVICES]
        else:
            services = [w.get('service') for w in defaults.FRONTEND_SERVICES]
        if not service_object.get('service') in services:
            raise Error("Error: the service attribute for each entry in an agave_{} config must be "
                        "in {}.".format(service_type, services))

        # Hierarchical derivation of properties -
        properties = self.get_all_properties_for_service(service_object['service'])
        for property in properties:
            # check for property already on service object itself
            if service_object.get(property):
                continue
            # if not there, check for property on server
            elif server.get(property):
                service_object[property] = server[property]
            # if not there, check for property within the args object (includes flattened configs)
            elif self.args.get(property):
                service_object[property] = self.args[property]
            # finally, get the default property
            else:
                try:
                    service_object[property] = self.get_default(service_object['service'], property)
                except Exception as e:
                    raise Error("Unable to get_default for property {} "
                                "and service_object {}.\n ".format(property, service_object))

    def audit_servers(self):
        """Derive the Agave services and basic attributes that will be run on each host.
        This function primarily provides default values for attributes that could be supplied by the user.
        The subsequent audit_configs_and_supply_defaults() below translates these attributes into properties that
        will be used to generate docker-compose files from templates in the Ansible role.
        """
        # first, we need to check for agave database roles as those will allow us to derive the IP:
        for s in self.servers:
            self.check_agave_db_role(s)

        # having iterated through the servers, we can now apply the final audits:
        self.audit_and_default_agave_db_attrs()

        # with the db roles (and associated properties) derived, we can now iterate through the servers again
        # looking for workers and frontends
        for s in self.servers:

            # loop through each server checking for the agave_workers and agave_frontends roles:
            if 'agave_workers' in s.get('roles'):
                # if a server is designated as a an agave_workers server but does not supply the agave_workers
                # attribute, all workers will be assigned to the server using the default configurations.
                if not 'agave_workers' in s.keys():
                    print("Warning: Using all default workers for server {}".format(s['uuid']))
                    s['agave_workers'] = defaults.WORKER_SERVICES
                if not type(s['agave_workers']) == list:
                    raise Error("Error: the agave_workers attribute must be a list. "
                                "Got type: {} instead.".format(type(s['agave_workers'])))
                for w in s['agave_workers']:
                    try:
                        self.derive_service_properties(w, "workers", s)
                    except Exception as e:
                        raise Error("Error: could not derive service properties "
                                    "for worker {} and server {}".format(w, s))
            else:
                # if the server did not have the agave_workers role, set to empty list.
                s['agave_workers'] = []
                print("Info: No agave workers for host {}".format(s['uuid']))

            if 'agave_frontends' in s.get('roles'):
                # if a server is designated as a an agave_frontends server but does not supply the agave_frontends
                # attribute, all frontends will be assigned to the server using the default configurations.
                if not 'agave_frontends' in s.keys():
                    print("Warning: Using all default frontends for server {}".format(s['uuid']))
                    s['agave_frontends'] = defaults.FRONTEND_SERVICES
                if not type(s['agave_frontends']) == list:
                    raise Error("Error: the agave_frontends attribute must be a list. "
                                "Got type: {} instead.".format(type(s['agave_frontends'])))
                for w in s['agave_frontends']:
                    try:
                        self.derive_service_properties(w, "frontends", s)
                    except Exception as e:
                        raise Error("Error: could not derive service properties "
                                    "for frontend {} and server {}".format(w, s))
                    # add proxy properties to server variables so they are available to compile the
                    # httpd.conf.j2 template.
                    if w['service'] == 'proxy':
                        s.update(w)
            else:
                # if the server did not have the agave_frontends role, set to empty list.
                s['agave_frontends'] = []
                print("Info: No agave frontends for host {}".format(s['uuid']))


    def audit_configs_and_supply_defaults(self):
        """This method converts properties that were established in the audit_servers() method into attributes
        that can be used directly by the Ansible role to generate docker-compose files for the various services.
        
        For each (agave) service defined on each server, the following types of attributes are generated:
        
            - environment: dictionary of key/value pairs comprising the environment for the docker container. The
                           keys are the variable names recognized by the agave services (typically, all uppercase).
            - ports: dictionary of key/value pairs comprising the host port and container port to expose for the 
                     docker container.
            - volumes: dictionary of host_path/container_path pairs of volumes to mount into the docker container.
            - name: name for the docker service.
            - image: docker image to use for the service.
            - 
        """
        for s in self.servers:
            # the final list of agave_services for this host; we will append finalized agave service objects to this
            # list as we iterate through the agave_workers and agave_frontends objects and do any necessary
            # translations.
            s['agave_services'] = []

            for w in s['agave_workers'] + s['agave_frontends']:
                # for each agave worker or frontend, we will define and append a new agave service, a dictionary with
                # the above attributes defined.
                agave_service = {'image': self.get_image(w['service']),
                                 # names of docker services cannot have '_' characters:
                                 'name': '{}.{}'.format(w['name'], s['host_id']).replace('_', ''),
                                 'environment': {},
                                 'ports': None,
                                 'volumes': None,
                                 }
                # to create the environment, we iterate over the keys of w and check the translation table to
                # see if a given key corresponds to a recognized environment variable:
                for key, value in w.items():
                    env_var = defaults.agave_environment_translations.get(key)
                    if env_var:
                        agave_service['environment'][env_var] = value
                # volumes -- no facility for overriding the defaults for now:
                volumes = self.get_volumes_for_service(w['service'])
                # ports -- no facility for overriding the defaults for now:
                ports = self.get_ports_for_service(w['service'])
                # for each service, we only add non-empty dictionaries since the templates key off the existence of
                # certain keys for writing out headers in the compose files
                if volumes:
                    agave_service['volumes'] = volumes
                if ports:
                    agave_service['ports'] = ports

                # append the final service
                s['agave_services'].append(agave_service)

    def do_action(self, action):
        actions = self.get_actions()
        if action not in actions:
            raise Error("Error: {} is not a valid agave action. Valid actions: {}".format(action, actions))
        if action == 'update_services':
            self.configs['remove_existing_services'] = True
        else:
            self.configs['remove_existing_services'] = False
        try:
            self.audit_servers()
        except Exception as e:
            raise Error("Error - Unable to audit servers; Info: {}".format(e))

        try:
            self.audit_configs_and_supply_defaults()
        except Exception as e:
            raise Error("Error - Unable to audit configs; Info: {}".format(e))

        # call the service_host playbook for these servers and configs
        commons = ['service_host', 'legacy_docker_daemon']
        role_map = {'agave_frontends': ['service_host', 'legacy_docker_host', 'agave_services'],
                    'agave_workers': ['service_host', 'legacy_docker_host', 'agave_services'],
                    'all_agave_dbs': ['service_host', 'legacy_docker_host', 'agave_mysql',
                                      'agave_mongo', 'agave_beanstalk'],
                    'agave_mysql': ['service_host', 'legacy_docker_host'],
                    'agave_mongo': ['service_host', 'legacy_docker_host',],
                    'agave_beanstalk': ['service_host', 'legacy_docker_host',]}
        self.apply_derived_roles(role_map=role_map)

        if self.deploy_persistence(action):
            self.derive_persistence_attrs()
            commons.append('persistence')
            self.configs['run_migrations'] = True
        else:
            self.configs['run_migrations'] = False

        # set version of docker:
        if not self.configs.get('dont_update_docker'):
            self.configs['update_docker_version'] = True
            self.configs['docker_version'] = '17.05.0.ce-1.el7.centos'
            self.configs['update_docker_version'] = self.update_docker(action)
            self.configs['docker_compose_version'] = '1.18.0'

        try:
            self.execute_common_playbooks(commons)
        except Exception as e:
            raise Error(msg="Unhandled exception executing common playbooks. Exception: {}".format(e))

        # call the actual agave playbook:
        self.execute_playbook('agave.plbk', keep_files=self.args['keep_tempfiles'])

