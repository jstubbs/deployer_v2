# Entrypoint for the jupyterhub project.

from util.project import Project, Error
from . import defaults

class Jupyterhub(Project):
    """
    Project class to handle JupyterHub deployment actions.
    """

    def get_actions(self):
        return ['deploy']

    def audit_servers(self):
        # exactly one host must be a jupyter manager
        if not self.check_host_role_exact(role='hub', num=1):
            raise Error("Error: exactly one server must occupy the role 'hub'.")
        for s in self.servers:
            if 'hub' in s.get('roles'):
                mgr_server = s
                break

        # derive fields from the manager ip:
        # if private_ip isn't specified, use the ssh_host IP:
        if not 'private_ip' in mgr_server.keys():
            mgr_server['private_ip'] = mgr_server['ssh_host']
            print("No private_ip specified for hub manager; using ssh_host: {}".format(mgr_server['ssh_host']))

        # at least one host must be a jupyter worker
        if not self.get_host_role_count(role='worker') >= 1:
            raise Error("Error: at least one server must occupy the role 'worker'.")
        for s in self.servers:
            if 'worker' in s.get('roles'):
                if 'private_ip' not in s.keys():
                    s['private_ip'] = s['ssh_host']
                    print("No private_ip specified for worker; using ssh_host: {}".format(s['ssh_host']))

    def audit_configs_and_supply_defaults(self):
        # derive various IP attributes:
        mgr_ip = self.get_hosts_role(role='hub')[0].get('private_ip')
        if 'hub_ip_for_notebooks' not in self.configs:
            self.configs['hub_ip_for_notebooks'] = mgr_ip
        if 'swarm_manager_ip' not in self.configs:
            self.configs['swarm_manager_ip'] = mgr_ip
        if 'nfs_server_ip' not in self.configs:
            self.configs['nfs_server_ip'] = mgr_ip

        # derive the tenant_id from the command given:
        self.configs['tenant_id'] = self.args.get('tenant')
        if not self.configs['tenant_id']:
            raise Error("Error: No tenant specified. A tenant is required for all jupyterhub actions.")

        # derive worker_tasks_file based on the tenant:
        self.configs['worker_tasks_file'] = self.args.get('worker_tasks_file', defaults.worker_tasks_file)
        if not self.args.get('worker_tasks_file'):
            if self.configs['tenant_id'] == 'designsafe':
                self.configs['worker_tasks_file'] = 'designsafe.yml'

        # host is required:
        self.configs['jupyterhub_host'] = self.args.get('jupyterhub_host')
        if not self.configs['jupyterhub_host']:
            raise Error("Error: No jupyterhub_host specified. A jupyterhub_host is required for "
                        "all jupyterhub actions.")
        if 'https' in self.configs['jupyterhub_host'] or '/' in self.configs['jupyterhub_host']:
            raise Error("Error: jupyterhub_host should only contain a domain, not `https` or "
                        "any '/' characters.")

        # oauth callback can be derived from the host:
        self.configs['jupyterhub_oauth_callback'] = self.args.get('jupyterhub_oauth_callback')
        if not self.configs['jupyterhub_oauth_callback']:
            self.configs['jupyterhub_oauth_callback'] = 'https://{}/hub/oauth_callback'.format(self.configs['jupyterhub_host'])

        # self-signed certs will be supplied if they are not provided as configs:
        self.configs['jupyterhub_cert'] = self.args.get('jupyterhub_cert')
        self.configs['jupyterhub_key'] = self.args.get('jupyterhub_key')
        if not self.configs['jupyterhub_cert'] or not self.configs['jupyterhub_key']:
            print("WARNING: no jupyterhub_cert/jupyterhub_key provided; supplying a self-signed certificate.")
            self.configs['jupyterhub_cert'] = defaults.jupyterhub_cert
            self.configs['jupyterhub_key'] = defaults.jupyterhub_key

        # use_stock_nfs should be true if the tenant is not using a custom NFS share for persistent user data directories.
        self.configs['use_stock_nfs'] = self.args.get('use_stock_nfs', defaults.use_stock_nfs)

        # volume_mounts - list of volumes to mount into user notebook servers
        self.configs['volume_mounts'] = self.args.get('volume_mounts', defaults.volume_mounts)

        # all jupyterhub's must specify OAuth credentials and a callback:
        self.configs['jupyterhub_oauth_client_id'] = self.args.get('jupyterhub_oauth_client_id')
        if not self.configs['jupyterhub_oauth_client_id']:
            raise Error("Error: No jupyterhub_oauth_client_id specified. A jupyterhub_oauth_client_id is required.")

        self.configs['jupyterhub_oauth_client_secret'] = self.args.get('jupyterhub_oauth_client_secret')
        if not self.configs['jupyterhub_oauth_client_secret']:
            raise Error("Error: No jupyterhub_oauth_client_secret specified. "
                        "A jupyterhub_oauth_client_secret is required.")
        self.configs['oauth_validate_cert'] = self.args.get('oauth_validate_cert', defaults.oauth_validate_cert)

        self.configs['jupyterhub_tenant_base_url'] = self.args.get('jupyterhub_tenant_base_url')
        if not self.configs['jupyterhub_tenant_base_url']:
            # try to get the tenant base url:
            try:
                self.configs['jupyterhub_tenant_base_url'] = self.get_tenant_url(self.args.get('tenant')).strip('https://')
            except:
                pass
            if not self.configs['jupyterhub_tenant_base_url']:
                raise Error("Error: No jupyterhub_tenant_base_url specified. A jupyterhub_tenant_base_url is required.")

        # jupyterhub image and jupyter_user image:
        self.configs['jupyterhub_image']= self.args.get('jupyterhub_image', defaults.jupyterhub_image)
        self.configs['jupyter_user_image'] = self.args.get('jupyter_user_image')
        if not self.configs['jupyter_user_image']:
            self.configs['jupyter_user_image'] = defaults.jupyter_user_image
            print("WARNING: no jupyter_user_image specified; supplying the default: {} but "
                  "this is probably not what you want.".format(defaults.jupyter_user_image))

        # derive the jupyterhub_config_py_template from the jupyterhub image:
        self.configs['jupyterhub_config_py_template'] = self.args.get('jupyterhub_config_py_template',
                                                                      defaults.jupyterhub_config_py_template)
        if not self.args.get('jupyterhub_config_py_template') and 'alpha' in self.configs['jupyterhub_image']:
            self.configs['jupyterhub_config_py_template'] = 'jupyterhub_config.py.j2'

        # derive the docker_compose_hub_file_name from the jupyterhub image:
        self.configs['docker_compose_hub_file_name'] = self.args.get('docker_compose_hub_file_name',
                                                                      defaults.docker_compose_hub_file_name)
        if not self.args.get('docker_compose_hub_file_name') and 'alpha' in self.configs['docker_compose_hub_file_name']:
            self.configs['docker_compose_hub_file_name'] = 'docker-compose-hub.yml.j2'

        # derive the button text from the image:
        self.configs['jupyterhub_login_button_text'] = self.args.get('jupyterhub_login_button_text', 'JupyterHub')
        # override the button text for the alpha images:
        if 'alpha' in self.configs['jupyterhub_image'] and not self.args.get('jupyterhub_login_button_text'):
            self.configs['jupyterhub_login_button_text'] = 'Log in'

        # Jupyterhubs can supply a /projects/v2 API to mount project directories in the user notebook server; several
        # configs are required:
        self.configs['mount_projects'] = self.args.get('mount_projects', defaults.mount_projects)
        if self.configs['mount_projects']:
            self.configs['host_projects_root_dir'] = self.args.get('host_projects_root_dir')
            if not self.configs['host_projects_root_dir']:
                raise Error("Error: mount_projects was True but host_projects_root_dir was not specified. "
                            "host_projects_root_dir is required to mount projects.")

            self.configs['container_projects_root_dir'] = self.args.get('container_projects_root_dir')
            if not self.configs['container_projects_root_dir']:
                self.configs['container_projects_root_dir'] = defaults.container_projects_root_dir
                print("WARNING: no container_projects_root_dir specified; "
                      "using {}".format(defaults.container_projects_root_dir))

        # Jupyterhubs can specify to check_homedir_exists - that is, make a call to the files API to check if
        self.configs['check_homedir_exists'] = self.args.get('check_homedir_exists', defaults.check_homedir_exists)
        if self.configs['check_homedir_exists']:
            self.configs['agave_storage_system'] = self.args.get('agave_storage_system')
            if not self.configs['agave_storage_system']:
                raise Error("Error: check_homedir_exists was true but agave_storage_system was not specified.")
        else:
            self.configs['agave_storage_system'] = ''

        # Hubs can specify whether to use TAS uid/gid in the container; if so, need to specify TAS credentials to
        # connect to the TAS API:
        self.configs['use_tas_uid'] = self.args.get('use_tas_uid', defaults.use_tas_uid)
        if self.configs['use_tas_uid']:
            self.configs['tas_role_account'] = self.args.get('tas_role_account', defaults.tas_role_account)
            self.configs['tas_role_pass'] = self.args.get('tas_role_pass')
            if not self.configs['tas_role_pass']:
                raise Error("Error: tas_role_pass is required if use_tas_uid is set to True.")
        else:
            # make sure tas_role_pass isn't in the configs because it can trigger hub to use TAS anyway:
            try:
                self.configs.pop('tas_role_pass')
            except KeyError:
                pass
            print('WARNING: Not using TAS UID/GID for this Hub; this means the container image UID and GID will be used.')
        # all instances use swarm at this point:
        if 'use_swarm' not in self.configs:
            self.configs['use_swarm'] = True

        # set version of docker:
        self.configs['update_docker_version'] = True
        self.configs['docker_version'] = '1.11.2-1.el7.centos'
        self.configs['update_docker_compose_version'] = True
        self.configs['docker_spawner_container_ip'] = '172.17.42.1'


        self.configs['remove_notebook_servers'] = self.args.get('remove_notebook_servers',
                                                                defaults.remove_notebook_servers)

        # logging
        self.configs['use_docker_splunk'] = self.args.get('use_docker_splunk', defaults.use_docker_splunk)
        if self.configs['use_docker_splunk']:
            self.configs['docker_splunk_token'] = self.args.get('docker_splunk_token')
            if not self.configs['docker_splunk_token']:
                raise Error("Error: docker_splunk_token required when use_docker_splunk set to True.")

    def do_action(self, action):
        actions = self.get_actions()
        if action not in actions:
            raise Error("Error: {} is not a valid jupyterhub action. Valid actions: {}".format(action, actions))
        role_map = {'hub': ['service_host', 'legacy_docker_host', 'swarm_manager', 'nfs_server'],
                    'worker': ['service_host', 'legacy_docker_host', 'swarm_worker', 'nfs_client']}

        # Need to do this check before we call audit_configs_and_supply_defaults so that we know how to create the
        # role_map.
        self.configs['mount_tacc_stockyard'] = self.args.get('mount_tacc_stockyard', defaults.mount_tacc_stockyard)
        if self.configs['mount_tacc_stockyard']:
            role_map['worker'].append('lustre')
        self.apply_derived_roles(role_map=role_map)
        try:
            self.audit_servers()
        except Exception as e:
            raise Error("Error - Unable to audit servers; Info: {}".format(e))
        # print("Servers after audit and application of derived roles: {}".format(self.servers))
        try:
            self.audit_configs_and_supply_defaults()
        except Exception as e:
            raise Error("Error - Unable to audit configs; Info: {}".format(e))

        # call the service_host playbook for these servers and configs
        commons = ['service_host', 'lustre', 'legacy_docker_daemon', 'swarm_manager', 'swarm_worker', 'nfs_cluster']
        try:
            self.execute_common_playbooks(commons)
        except Exception as e:
            raise Error(msg="Unhandled exception executing common playbooks. Exception: {}".format(e))

        # call the actual jupyter playbook:
        self.execute_playbook('jupyterhub.plbk', keep_files=self.args['keep_tempfiles'])